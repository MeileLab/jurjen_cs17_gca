#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include <cvode/cvode.h>               /* prototypes for CVODE fcts., consts.  */
#include <nvector/nvector_serial.h>    /* access to serial N_Vector            */
#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix            */
#include <sunlinsol/sunlinsol_dense.h> /* access to dense SUNLinearSolver      */
#include <cvode/cvode_direct.h>        /* access to CVDls interface            */
#include <sundials/sundials_types.h>   /* defs. of realtype, sunindextype      */
#include <sundials/sundials_math.h>

//number of equations is the number of chemical species
#define NEQ 11
//number of reaction pathways that can be returned (to calculate mass balances)
#define NREAC 20

/* scalar absolute tolerances */
#define ATOL_OrgA RCONST(1.0e-5)
#define ATOL_OrgB RCONST(1.0e-5)
#define ATOL_OrgC RCONST(1.0e-5)
#define ATOL_O2 RCONST(1.0e-7)
#define ATOL_FeSolidA RCONST(1.0e-5)
#define ATOL_FeSolidB RCONST(1.0e-5)
#define ATOL_SO4 RCONST(1.0e-6)
#define ATOL_CH4 RCONST(1.0e-6)
#define ATOL_HS RCONST(1.0e-6)
#define ATOL_Fe RCONST(1.0e-6)
#define ATOL_FeSn RCONST(1.0e-5)

/* scalar relative tolerance (not used) */
#define RTOL RCONST(1.0e-6)

#define ZERO RCONST(0.0)
#define Ith(v,i)    NV_Ith_S(v,i)       /* Ith numbers components 0..(NEQ-1) */

/* model parameters */
#define NPAR 17
static realtype parms[NPAR];

#define kOrgA parms[0]
#define kOrgB parms[1]
#define kOrgC parms[2]
#define KmO2 parms[3]
#define KmFeSolid parms[4]
#define KmSO4 parms[5]
#define KmSO4AOM parms[6]
#define k5 parms[7]
#define k6 parms[8]
#define k7 parms[9]
#define k8 parms[10]
#define k9a parms[11]
#define k9b parms[12]
#define k10 parms[13]
#define k11 parms[14]
#define k12 parms[15]
#define n parms[16]

//the order for chemical species
#define OrgA 0
#define OrgB 1
#define OrgC 2
#define O2 3
#define FeSolidA 4
#define FeSolidB 5
#define SO4 6
#define CH4 7
#define HS 8
#define Fe 9
#define FeSn 10

//structure fdata vector
#define fdata_por_index 0 //porosity
#define fdata_issia_index 1 //flag if sia is used
#define fdata_returnrates_index 2 //flag if rates should be returned
static int fdata_reac_index; //beginning index of reaction vector
static int fdata_dtrans_index;
static int size_fdata;

/* Private function to check function return values */
static int check_flag(void *flagvalue, char *funcname, int opt);

/* Function Called by the Solver */
static int f(realtype t, N_Vector u, N_Vector udot, void *f_data);

//function to call vode, when SIA is used fill dCdt_trans,
// otherwise leave it as NULL
void call_vode(realtype * conc_vec, realtype * dCdt_trans,
               realtype dt, realtype por, realtype * reac_vec,
               int * is_sia, int * return_rates) {
  realtype reltol, t, tout;
  N_Vector y, abs_tol; 
  SUNMatrix A;
  SUNLinearSolver LS;
  void * cvode_mem;
  long int mxsteps;
  int flag;    
  
  y = abs_tol = NULL;
  cvode_mem = NULL;
  A = NULL;
  LS = NULL;
  mxsteps = 4e3;
  reltol = RTOL; //ZERO to switch off    
  
  //set absolute tolerances
  abs_tol = N_VNew_Serial(NEQ);
  Ith(abs_tol, OrgA) = ATOL_OrgA;
  Ith(abs_tol, OrgB) = ATOL_OrgB;
  Ith(abs_tol, OrgC) = ATOL_OrgC;
  Ith(abs_tol, O2) = ATOL_O2;
  Ith(abs_tol, FeSolidA) = ATOL_FeSolidA;
  Ith(abs_tol, FeSolidB) = ATOL_FeSolidB;
  Ith(abs_tol, SO4) = ATOL_SO4;
  Ith(abs_tol, CH4) = ATOL_CH4;
  Ith(abs_tol, HS) = ATOL_HS;
  Ith(abs_tol, Fe) = ATOL_Fe;
  Ith(abs_tol, FeSn) = ATOL_FeSn;
  
  /* Create a serial vector */
  y = N_VNew_Serial(NEQ);  /* Allocate u vector */
  if(check_flag((void*)y, "N_VNew_Serial", 0))
    exit(1);
  
  /* set initial conditions in u vector */
  for (int i_spec = 0; i_spec < NEQ; i_spec++) {
    Ith(y, i_spec) = conc_vec[i_spec];
  }
  
  /*
   Call CvodeCreate to create integrator memory
   
   CV_BDF     specifies the Backward Differentiation Formula
   CV_NEWTON  specifies a Newton iteration
   
   A pointer to the integrator problem memory is returned and
   stored in cvode_mem.
   */
  cvode_mem = CVodeCreate(CV_BDF);
  if (check_flag((void *)cvode_mem, "CVodeCreate", 0))
    exit(1);
  
  /* Call CVodeInit to initialize the integrator memory and specify the
   * user's right hand side function in y'=f(t,y), the inital time ZERO, and
   * the initial dependent variable vector y. */
  flag = CVodeInit(cvode_mem, f, ZERO, y);
  if (check_flag(&flag, "CVodeInit", 1)) 
    exit(1);
  
  /* Call CVodeSVtolerances to specify the scalar relative
   * and vector absolute tolerances */
  flag = CVodeSVtolerances(cvode_mem, reltol, abs_tol);
  if (check_flag(&flag, "CVodeSStolerances", 1)) 
    exit(1);
  
  //Set the maximum number of steps (standard 500)
  flag = CVodeSetMaxNumSteps(cvode_mem, mxsteps);
  if(check_flag(&flag, "CVodeSetMaxNumSteps", 1)) 
    exit(1);
  
  /* Set the pointer to user-defined data */
  //note that sia_method == 0 when it is not specified
  realtype extra_info[size_fdata];
  extra_info[fdata_por_index] = por;
  extra_info[fdata_issia_index] = (realtype) is_sia[0]; //bool in realtype
  extra_info[fdata_returnrates_index] = (realtype) return_rates[0]; //bool in realtype 
  if (*is_sia) {
    for (int i_spec = 0; i_spec < NEQ; ++i_spec) { //the transport time derivatives
      extra_info[fdata_dtrans_index + i_spec] = dCdt_trans[i_spec];
    }
  }
  
  flag = CVodeSetUserData(cvode_mem, extra_info);
  if (check_flag(&flag, "CVodeSetUserData", 1))
    exit(1);
  
  tout = dt; //output time
  
  /* Create dense SUNMatrix for use in linear solves */
  A = SUNDenseMatrix(NEQ, NEQ);
  if(check_flag((void *)A, "SUNDenseMatrix", 0)) 
    exit(1);
  
  /* Create dense SUNLinearSolver object for use by CVode */
  LS = SUNDenseLinearSolver(y, A);
  if(check_flag((void *)LS, "SUNDenseLinearSolver", 0)) 
    exit(1);
  
  /* Call CVDlsSetLinearSolver to attach the matrix and linear solver to CVode */
  flag = CVDlsSetLinearSolver(cvode_mem, LS, A);
  if(check_flag(&flag, "CVDlsSetLinearSolver", 1)) 
    exit(1);
  
  //run vode
  flag = CVode(cvode_mem, tout, y, &t, CV_NORMAL);
  
  int successfull_integration = 0;
  //check for errors
  if(!check_flag(&flag, "CVode", 1)) { //successfull integration
    //copy results
    for (int i_spec = 0; i_spec < NEQ; ++i_spec) {
      conc_vec[i_spec] = Ith(y, i_spec);
    }
    successfull_integration = 1;
  } else {
    conc_vec[0] = -1.2345678;
  }
  
  if (return_rates[0]) {
    for (int i_reac = 0; i_reac < NREAC; ++i_reac) {
      reac_vec[ i_reac ] = extra_info[ fdata_reac_index + i_reac ];	
    }
  }
  
  //free data
  N_VDestroy_Serial(y);   /* Free the u vector */
  CVodeFree(&cvode_mem);  /* Free the integrator memory */
  /* Free the linear solver memory */
  SUNLinSolFree(LS);
  /* Free the matrix memory */
  SUNMatDestroy(A);
  
  N_VDestroy_Serial(abs_tol);
}

//solve all concentrations in a domain serially
//conc = concentration vector of all chemicals
//dCdt_trans is change in concentration due to transport
//por is porosity at each node
void solve_vec(double * conc, int * ncells, double * dt,
               double * parameters, double * dCdt_trans,
               double * por, double * reac,
               int * is_sia, int * return_rates) {
  //convert timestep to realtype
  realtype timestep = (realtype) dt[0];
  
  //following variable declarations store data of one cell
  int begin_spec[ NEQ ]; //indexes of concentrations
  int begin_reac[ NREAC ];
  realtype conc_vec[ NEQ ]; //concentrations of all species
  realtype reac_vec[ NREAC ];
  realtype dCdt_trans_vec[ NEQ ]; //the rates associated to transport
  
  //copy parameters to global static scope
  for (int i_par = 0; i_par < NPAR; i_par++) {
    parms[i_par] = (realtype) parameters[i_par];
  }
  
  //set indexes for fdata structure (don't worry about numbers if function not used)
  fdata_reac_index = fdata_returnrates_index + *return_rates;
  fdata_dtrans_index = fdata_returnrates_index + return_rates[0] * NREAC + is_sia[0];
  size_fdata = fdata_returnrates_index + return_rates[0] * NREAC + is_sia[0] * NEQ;
  
  //set pointers to the beginning of each species
  //dCdt_trans and conc have same structure and use begin_spec for indexing
  begin_spec[0] = 0;
  for (int i = 1; i < NEQ; ++i) {
    begin_spec[i] = begin_spec[i-1] + ncells[0];
  }
  //do the same for reactions
  if (return_rates[0]) {
    begin_reac[0] = 0;
    for (int i = 1; i < NREAC; ++i) {
      begin_reac[i] = begin_reac[i-1] + ncells[0];
    }
  }
  
  //loop to call vode for all cells
  for (int i_cell = 0; i_cell < ncells[0]; ++i_cell) {
    for (int i_spec = 0; i_spec < NEQ; i_spec++) {
      conc_vec[i_spec] = (realtype)conc[begin_spec[i_spec]];
    }
    
    if (is_sia[0]) {
      for (int i_spec = 0; i_spec < NEQ; ++i_spec) {
        dCdt_trans_vec[i_spec] = (realtype)dCdt_trans[begin_spec[i_spec]];
      }
    }
    
    realtype por_i = (realtype) por[i_cell];
    
    
    call_vode(conc_vec, dCdt_trans_vec, timestep, por_i,
              reac_vec, is_sia, return_rates);
    if (conc_vec[0] == -1.2345678) { //it failed
      conc[0] = -1.2345678;
      break;
    } else {
      //copy results and increment indices for next iteration
      for (int i_spec = 0; i_spec < NEQ; ++i_spec) {
        conc[begin_spec[i_spec]] = (double)conc_vec[i_spec];
        begin_spec[i_spec] = begin_spec[i_spec] + 1;
      }
      
      if (return_rates[0]) {
        for (int i_reac = 0; i_reac < NREAC; ++i_reac) {
          reac[begin_reac[i_reac]] = (double)reac_vec[i_reac];
          begin_reac[i_reac] = begin_reac[i_reac] + 1;
        }
      }
    }
  }
}

//appears to need a main function to compile
int main(void) {
  printf("This program is rather pointless.");
}

//heaviside operator
realtype heavi_op(realtype x) {
  return(1.0 / (1.0 + SUNRexp(-100 * x)));
}

/*
 *-------------------------------
 * Functions called by the solver
 *-------------------------------
 */

/* f routine. Compute f(t,u). (the function with pde's that needs to be integrated) */
static int f(realtype t, N_Vector u, N_Vector udot, void *f_data)
{ 
  realtype * conc = NV_DATA_S(u); //state vars
  realtype * dconc = NV_DATA_S(udot); //derivatives
  realtype * reac, * dtrans;
  
  //extra info about method and communication
  realtype * extra_info = (realtype *)f_data;
  realtype por = extra_info[fdata_por_index];
  int is_sia_bool = (int)extra_info[fdata_issia_index];
  int return_rates = (int)extra_info[fdata_returnrates_index];
  
  //variables to store and calculate reaction rates
  realtype R1, R2, R3, R4, R5, R6, R7, R8, R9, R9a, R9b, R10, R10a, R10b, R10c, R11, R12;
  realtype InhibO2, InhibFe, InhibS;
  realtype R_Aer_OrgA, R_Aer_OrgB, R_Aer_OrgC, R_Anaer_OrgA, R_Anaer_OrgB;
  
  //solid and solute volume fractions conversion factors
  realtype conv_sed_to_tot, conv_pw_to_tot, conv_tot_to_sed, conv_tot_to_pw, nc_ratio, sc_ratio;
  conv_sed_to_tot = 1.0-por;
  conv_pw_to_tot = por;
  conv_tot_to_sed = 1.0/conv_sed_to_tot;
  conv_tot_to_pw = 1.0/conv_pw_to_tot;
  
  nc_ratio = 16.0/106.0; //Redfield stoichiometry of organic matter
  
  conc[O2] = SUNMAX(0.0, conc[O2]); //to make sure reaction rates cannot be negative
  
  //Inhibition terms organic matter remineralization
  InhibO2 = KmO2 / (KmO2 + conc[O2]);
  InhibFe = KmFeSolid / (conc[FeSolidA] + KmFeSolid);
  InhibS = KmSO4 / (conc[SO4] + KmSO4);
  
  //Aerobic mineralization
  R_Aer_OrgA = 1e2 * kOrgA * conc[OrgA] * conv_sed_to_tot * (1.0 - InhibO2);
  R_Aer_OrgB = 1e2 * kOrgB * conc[OrgB] * conv_sed_to_tot * (1.0 - InhibO2);
  R_Aer_OrgC = 1e2 * kOrgC * conc[OrgC] * conv_sed_to_tot * (1.0 - InhibO2);
  
  R1 = R_Aer_OrgA + R_Aer_OrgB + R_Aer_OrgC;
  
  //Anaerobic mineralization
  R_Anaer_OrgA = kOrgA * conc[OrgA] * InhibO2 * conv_sed_to_tot;
  R_Anaer_OrgB = kOrgB * conc[OrgB] * InhibO2 * conv_sed_to_tot;
  
  //calculate reaction rates (per total volume)
  //reactions [mol/m3/yr] (rate per total volume)
  R2 = (R_Anaer_OrgA + R_Anaer_OrgB) * InhibO2 * (1.0 - InhibFe); //mol C
  R3 = (R_Anaer_OrgA + R_Anaer_OrgB) * InhibO2 * InhibFe * (1.0 - InhibS); //mol C
  R4 = (R_Anaer_OrgA + R_Anaer_OrgB) * InhibO2 * InhibFe * InhibS; //mol C
  
  R5 = k5 * conc[O2] *conc[CH4] * conv_pw_to_tot; //mol C
  R6 = k6 * (conc[SO4] / (conc[SO4] + KmSO4AOM)) * conc[CH4] * conv_pw_to_tot; //mol C
  R7 = k7 * conc[O2] * conc[HS] * conv_pw_to_tot; //mol S
  R8 = k8 * conc[O2] * conc[Fe] * conv_pw_to_tot; //mol Fe
  R9a = k9a * conc[FeSolidA] * conc[HS] * conv_sed_to_tot; //mol S
  R9b = k9b * conc[FeSolidB] * conc[HS] * conv_sed_to_tot; //mol S
  R9 = R9a + R9b; //mol S
  
  R10 = k10 * conc[Fe] * conc[HS] * conv_sed_to_tot;
  R10a = R10 * conc[FeSolidA] / (KmFeSolid + conc[FeSolidA]);
  R10b = (R10 - R10a) * conc[SO4] / (conc[SO4] + KmSO4);
  R10c = R10 - R10a - R10b;
  
  R11 = k11 * conc[FeSn] * conc[O2] * conv_sed_to_tot;
  R12 = k12 * conc[FeSolidA] * conv_sed_to_tot;
  
  //dC/dt of each species, which is the sum of reactions
  dconc[OrgA] = (-R_Aer_OrgA - R_Anaer_OrgA) * conv_tot_to_sed;
  dconc[OrgB] = (-R_Aer_OrgB - R_Anaer_OrgB) * conv_tot_to_sed;
  dconc[OrgC] = (-R_Aer_OrgC) * conv_tot_to_sed;
  dconc[O2] = (-R1 * (1.0 + 2.0 * nc_ratio) - 2.0 * R5 - 2.0 * R7 - 0.25 * R8 - (0.5+1.5*n) *R11 ) * conv_tot_to_pw;
  dconc[FeSolidA] = ( - 4.0 * R2 + R8 - R9a - 2.0*(n-1.0) * R10a - R12) * conv_tot_to_sed;
  dconc[FeSolidB] = (- R9b + R12) * conv_tot_to_sed;
  dconc[SO4] = (-0.5 * R3 - R6 + R7 + 0.125 * R9 - 0.25*(n-1.0) * R10b + n * R11 ) * conv_tot_to_pw;
  dconc[CH4] = (0.5 * R4 - R5 - R6 + 0.25*(n-1.0) * R10c) * conv_tot_to_pw;
  dconc[HS] = (0.5 * R3 + R6 - R7 - 0.125 * R9 - n * R10a - 0.25*(1.0+3.0*n) * R10b - n * R10c ) * conv_tot_to_pw;
  dconc[Fe] = (4.0 * R2 - 2.0 * R8 + R9 + (2.0 * n - 3) * R10a - R10b - R10c + R11) * conv_tot_to_pw;
  dconc[FeSn] = (R10 - R11) * conv_tot_to_sed;
  
  //return information about reaction rates
  if (return_rates) {
    reac = &extra_info[fdata_reac_index];
    
    //set individual reaction rates
    int i_reac = 0;
    realtype MinAFrac, MinBFrac;
    MinAFrac = R_Anaer_OrgA + R_Anaer_OrgB;
    if (MinAFrac > 0.0) {
      MinAFrac = R_Anaer_OrgA / MinAFrac;
      MinBFrac = 1.0 - MinAFrac;
    } else { //probably not necessary, but just to be sure
      MinAFrac = 0.0;
      MinBFrac = 0.0;
    }
    
    //return rates in mol per total volume per time
    reac[i_reac] = R_Aer_OrgA;         	  i_reac++; //R1a
    reac[i_reac] = R_Aer_OrgB;          	i_reac++; //R1b
    reac[i_reac] = R_Aer_OrgC;            i_reac++; //R1c
    reac[i_reac] = MinAFrac * R2;         i_reac++; //R2a
    reac[i_reac] = MinBFrac * R2;         i_reac++; //R2b
    reac[i_reac] = MinAFrac * R3;         i_reac++; //R3a
    reac[i_reac] = MinBFrac * R3;         i_reac++; //R3b
    reac[i_reac] = MinAFrac * R4;         i_reac++; //R4a
    reac[i_reac] = MinBFrac * R4;         i_reac++; //R4b
    reac[i_reac] = R5;          		      i_reac++; 
    reac[i_reac] = R6;          		      i_reac++; 
    reac[i_reac] = R7;          		      i_reac++; 
    reac[i_reac] = R8;          		      i_reac++; 
    reac[i_reac] = R9a;        		        i_reac++; 
    reac[i_reac] = R9b;        		        i_reac++; 
    reac[i_reac] = R10a;        		      i_reac++; 
    reac[i_reac] = R10b;        		      i_reac++; 
    reac[i_reac] = R10c;        		      i_reac++;
    reac[i_reac] = R11;         		      i_reac++;
    reac[i_reac] = R12;         		      i_reac++;
  }
  
  //add time derivative for transport as a scalar
  if (is_sia_bool) {
    //obtain time-derivative of the transport terms
    realtype * dtrans = &extra_info[fdata_dtrans_index];
    
    //add derivative from transport to dCdt vector
    for (int i_spec = 0; i_spec < NEQ; ++i_spec ) {
      dconc[i_spec] = dconc[i_spec] + *dtrans;
      dtrans++;
    }
  }
  
  return(0);
}

/* Check function return value...
 opt == 0 means SUNDIALS function allocates memory so check if
 returned NULL pointer
 opt == 1 means SUNDIALS function returns a flag so check if
 flag >= 0
 opt == 2 means function allocates memory so check if returned
 NULL pointer */
static int check_flag(void *flagvalue, char *funcname, int opt)
{
  int *errflag;
  
  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if (opt == 0 && flagvalue == NULL) {
    fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return(1);
  }
  
  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
              funcname, *errflag);
      return(1);
    }
  }
  
  /* Check if function returned NULL pointer - no memory allocated */
  else if (opt == 2 && flagvalue == NULL) {
    fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return(1);
  }
  
  return(0);
}
