This code is distributed on an "as is" basis, without warranties or conditions of any kind - without even the implied warranty of merchantability or fitness for a particular purpose.

Directory structure:
./Model/
./Model/main_CS17.R: script to run model for CS17
./Model/model_implementation.R: part of the model implementation called by the main script
./Model/speedy_reactions_CS17.c: Code to integrate reaction rates over time with cvode ode solver
./Model/PlotScript_CS17.R: Script to plot concentration profiles
./Model/integrated_rates_CS17.R: Script to integrate stored reaction rates over depth and to calculate and plot mass balances.

./Data/
./Data/CS17_por2.csv: Measured porosity profile (fitted in main_CS17.R)
./Data/CS17_sedages.csv: Determined ages of the sediment (used in main_CS17.R to calculate the sediment accumulation rate)
./Data/*.csv: Measured concentrations various chemical (used in PlotScript_CS17.R for comparison)

./Output/CS17_highres/
./Output/CS17_highres/year*.rda: Computed concentrations and reaction rates at times specified in the vector "output_times" in main script. (files are produced during a simulation)
./Output/intrates_*.csv/: Integrated reaction rates computed for each chemical with integrated_rates_CS17.R. (files can be produced by the script to integrate rates)

./JurRTM/: Contains code to solve partial differential equations for transport and reactions, which invoked by the model scripts main_CS17.R and model_implementation.R.
./JurRTM/doc/manual.pdf: A manual for the JurRTM directory


Running the model:
- Make sure R is installed. Code has been tested with R 3.6.3. It may also be convenient to install Rstudio or an other R environment.
- First C code needs to be compiled (see below)
- Open ./Model/main_CS17.R
- Change the following directory paths:
	* core_code_path (path should lead to JurRTM folder)
	* workdir_path (should be point to CS17 folder, which contains the Model, Data, and Output folders)
	* data_folder: choose a different name, so that stored results are not overwritten. If the folder does not exist yet it is created by the script.
- Run the code:
	* From terminal: Rscript --vanilla main_CS17.R (in bash it may be recommendable to run nohup Rscript --vanilla main_CS17.R & )
	* In Rstudio: ctrl+alt+R or in menu bar: code->Source. Rstudio often crashes, so it is better to run a real simulation from the terminal.
	* To start simulation from time=0 set the variable i_time to 2 (default, line ~ 227 in section Time Controls). Simulation can also start from a stored output file. For that change i_time to the year from which the simulation needs to be started, e.g. 135000 to load year135000.rda and start the simulation from that time.
	
Showing results:
- PlotScript_CS17.R provides a user interface to plot concentrations. To run the script the variables and functions from main_CS17.R need to be in the memory. To load it, it is easiest to start a simulation and stop it almost immediately (when it enters the main loop). When the C code has not been compiled, the user can simply comment out the lines that load the libraries (speedy_reactions_CS17 and matrices_and_solvers) and run the main_CS17 code. This will produce an error that can be ignored.
- integrated_rates_CS17.R can be used to plot mass balances. The code should be run with the variables and functions from main_CS17.R loaded (see previous point). To change the chemical species of which the mass balances plotted, change the name on line 182. To see the names of chemical species, run names(state_vars). To plot a new mass balance after the mass balances have already been plotted, run lines 184-222 (in Rscript simply put cursor on line 184 and hit ctrl+enter).
	
	
Compiling C code (linux/Unix/Mac):
- Go in terminal to ./JurRTM/C folder (cd /home/username/somewhere/JurRTM/C)
- Run R CMD SHLIB matrices_and_solvers.c
- Install cvode from SUNDIALS v4.1.0 (make sure the correct version is used)
- Compile speedy_reactions_CS17.c. With gcc use the flags -fPIC and -shared. Other flags depend on the machine. 
The easiest way to find out which flags should be used on a machine is to first build the examples from sundials as described in the sundials manual, i.e. go in the terminal to /somewhere/examples/cvode/serial and compile the source code using make or cmake. The commands and flags used to compile the code examples will be printed to the screen. Copy the commands to a new script in the same folder as where as speedy_reactions_CS17.c is located, and change the filename and make sure a shared library is built (i.e. add -shared and -fPIC flags). The bash script (ending in .sh) that was used to compile speedy_reactions by the author reads:
filename="speedy_reactions_CS17"
/usr/bin/cc -O3 -DNDEBUG -O3 -DNDEBUG -I/home/jurjen/Downloads/cvode4/instdir/include -c -fPIC ${filename}.c
/usr/bin/cc ${filename}.o -shared -o ${filename}.so  -O3 -DNDEBUG  -I/home/jurjen/Downloads/cvode4/instdir/include -L/home/jurjen/Downloads/cvode4/instdir/lib -lsundials_cvode -lsundials_nvecserial -lm /usr/lib/x86_64-linux-gnu/librt.so -Wl,-rpath,/home/jurjen/Downloads/cvode4/instdir/lib
Then run this script, e.g. if it's names compile.sh, first run chmod +x compile.sh and then ./compile.sh

Compiling C code in Windows (only general directions):
- Make sure you have a compiler installed. Here it will be assumed that the Rtools has been installed. For this go to https://cran.r-project.org/bin/windows/Rtools/ and carefully follow the installation instructions.
- Replace in the main file the lines to load "matrices_and_solvers.so" by
#Load libraries written in C to run the model faster
pathC = paste(core_code_path, "C/", sep = "")
system(paste("R CMD SHLIB ", pathC, "matrices_and_solvers.c", sep = "")) 
dyn.load(paste(pathC, "matrices_and_solvers", .Platform$dynlib.ext, sep = ""))
- The most tedious part is to compile cvode and speed_reactions_CS17.c. For this a new R package needs to be created, but this falls outside the scope of this document.  
 
