/*
 *
 * This code is distributed on an "as is" basis, without warranties or
 * conditions of any kind - without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <R.h>
#include <R_ext/Utils.h>

#define max(a,b) \
({ __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })

#define IGNORE -1.2345678

void R_CheckUserInterrupt(void);

//Thomas algorithm
void solveTri(int *nin, double *A, double *B, double *x) {
    
    //copy A and B, since we do not want to change them
    double A_copy[nin[0]*3-2];
    double B_copy[nin[0]];
    memcpy(A_copy, A, sizeof(A_copy));
    memcpy(B_copy, B, sizeof(B_copy));
    
    //forward elimination
    for (int j_row = 1; j_row < nin[0]; j_row++) {
        double xmult = A_copy[j_row*3-1] / A_copy[j_row*3-3];
        A_copy[j_row*3] = A_copy[j_row*3] - xmult * A_copy[j_row*3-2];
        B_copy[j_row] = B_copy[j_row] - xmult * B_copy[j_row-1];
    }
    
    //backward substitution
    x[nin[0]-1] = B_copy[nin[0]-1] / A_copy[nin[0]*3-3];
    for (int j_row = (nin[0]-2); j_row > -1; j_row--) {
        x[j_row] = (B_copy[j_row] - A_copy[j_row*3+1] * x[j_row+1]) /
        A_copy[j_row*3];
    }
}

void testDiagonalDominance(int *nin, double *A, int *res) {
    //first row
    *res = 1; //TRUE, it is diagonally dominant
    if (fabs(A[1]) > fabs(A[0])) {
        *res = 0;
    } else for (int j_row = 1; j_row < (nin[0] - 1); j_row++) {
        if ((fabs(A[j_row*3]) < fabs(A[j_row*3-1])) || (fabs(A[j_row*3]) <
                                                        fabs(A[j_row*3+1]))) {
            *res = 0;
            break;
        }
    }
    if (fabs(A[3 * (nin[0]-1)]) < fabs(A[3 * (nin[0]-1)-1])) {
        *res = 0;
    }
}

//nrow and ncol are dimensions of 2D grid
//A_mats_horz are all matrices for all rows
//A_mats similar, but then for columns
//B_mat contains the source term (length = ncol * nrow)
//conc are the initial guesses, i.e. x in Ax = B
//solveCols is 1 to solve columns, is 0 solve rows
//sweep_direction: 0 = downwards, 1 = upwards, 2 = alternating
void solve2D(int *nrow, int *ncol, double *A_mats_horz,
             double *A_mats_vert, double *B_mat,
             double * init_guess, int *max_iterations, double *rel_tol,
             int * solveCols, int * sweep_direction) {
    
    int ny, nx, length_mathorz, length_matvert;
    double *Ah, *Av, * conc, * conc_copy, * B_org;
    
    conc_copy = (double *)malloc(sizeof(double) * nrow[0] * ncol[0]);
    
    if (*solveCols == 1) { //columns are solved, info from rows in B
        ny = *nrow;
        nx = *ncol;
        
        Ah = A_mats_horz;
        Av = A_mats_vert;
        
        conc = init_guess;
        memcpy(conc_copy, conc, sizeof(double)*nx*ny);
        
        B_org = B_mat;
    } else { //rows are solved, info from columns in B
        nx = *nrow;
        ny = *ncol;
        
        Av = A_mats_horz;
        Ah = A_mats_vert;
        
        conc = (double *)malloc(sizeof(double) * nrow[0] * ncol[0]);
        B_org = (double *)malloc(sizeof(double) * nrow[0] * ncol[0]);
        
        //transpose rows and columns
        double * cc_ptr = conc_copy;
        double * b_ptr = B_org;
        for (int i_row = 0; i_row < *nrow; ++i_row) {
            for (int j_col = 0; j_col < *ncol; ++j_col) {
                *cc_ptr = init_guess[j_col * (*nrow) + i_row];
                cc_ptr++;
                *b_ptr = B_mat[j_col * (*nrow) + i_row];
                b_ptr++;
            }
        }
        memcpy(conc, conc_copy, sizeof(double)*nx*ny);
    }
    
    //number of elmements in an A matrix
    length_mathorz = nx * 3 - 2;
    length_matvert = ny * 3 - 2;
        
    //adapt vertical matrix to account for east west fluxes
    //following adds P cell from horz mats into vert mat
    for (int j_row = 0; j_row < ny; j_row++) {
        for (int i_col = 0; i_col < nx; i_col++) {
            int Pv = i_col * length_matvert + j_row * 3;
            int Ph = j_row * length_mathorz + i_col * 3;
            Av[Pv] = Av[Pv] + Ah[Ph];
        }
    }
    
    //vector B needs to be adapted to add east west fluxes
    double * B_col = (double *)malloc(sizeof(double) * ny);
    
    //start to solve 2D system iteratively
    for (int i_it = 0; i_it < *max_iterations; i_it++) {
        if (*sweep_direction == 0 || *sweep_direction == 2) { //downwards
            int startA = 0; //begin of A matrix for a column in 2D field
            int startB = 0; //idem for B vector of a column
            
            //do first column (only east flux)
            for (int j_row = 0; j_row < ny; j_row++) {
                B_col[j_row] =  B_org[j_row] - conc[ny + j_row] * Ah[length_mathorz * j_row + 1];
            }
            //solve first column
            solveTri( &ny, //nin = number of rows square mat
                     &Av[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
            startA = startA + length_matvert;
            startB = startB + ny;
            
            //do middle columns (west and east flux)
            for (int i_col = 1; i_col < (nx-1); i_col++) {
                for (int j_row = 0; j_row < ny; j_row++) {
                    B_col[j_row] =  B_org[startB + j_row] - conc[(i_col+1) * ny + j_row] * Ah[length_mathorz*j_row + 3*i_col +1] - conc[(i_col-1) * ny + j_row] * Ah[length_mathorz*j_row + 3*i_col -1];
                }
                //solve single column
                solveTri( &ny, //nin = number of rows square mat
                         &Av[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]); //pointer to begin conc
                startA = startA + length_matvert;
                startB = startB + ny;
            }
            
            //do last column (only west flux)
            for (int j_row = 0; j_row < ny; j_row++) {
                B_col[j_row] = B_org[startB + j_row] - conc[(nx-2) * ny + j_row] * Ah[length_mathorz*j_row + 3*(nx-1) -1];
            }
            //solve last column
            solveTri( &ny, //nin = number of rows square mat
                     &Av[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
        }
        //solve in reverse direction (upwards
        if (*sweep_direction == 1 || *sweep_direction == 2) {
            int startA = (nx-1) * length_matvert;
            int startB = (nx-1) * ny;
            
            //do last column (only west flux)
            for (int j_row = 0; j_row < ny; j_row++) {
                B_col[j_row] = B_org[startB + j_row] - conc[(nx-2) * ny + j_row] * Ah[length_mathorz*j_row + 3*(nx-1) -1];
            }
            //solve last column
            solveTri( &ny, //nin = number of rows square mat
                     &Av[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
            startA = startA - length_matvert;
            startB = startB - ny;
            
            //do middle columns (west and east flux)
            for (int i_col = (nx-2); i_col > 0; i_col--) {
                for (int j_row = 0; j_row < ny; j_row++) {
                    B_col[j_row] =  B_org[startB + j_row] - conc[(i_col+1) * ny + j_row] * Ah[length_mathorz*j_row + 3*i_col +1] - conc[(i_col-1) * ny + j_row] * Ah[length_mathorz*j_row + 3*i_col -1];
                }
                //solve single column
                solveTri(&ny, //nin = number of rows square mat
                         &Av[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]); //pointer to begin conc
                startA = startA - length_matvert;
                startB = startB - ny;
            }
            
            //do first column (only east flux)
            for (int j_row = 0; j_row < ny; j_row++) {
                B_col[j_row] =  B_org[j_row] - conc[ny + j_row] * Ah[length_mathorz * j_row + 1];
            }
            //solve first column
            solveTri(&ny, //nin = number of rows square mat
                     &Av[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
        }
        
        //calculate largest relative error and copy conc into conc_copy
        double rel_error = 0;
        for (int i = 0; i < nx*ny; i++) {
            rel_error = max(rel_error, fabs(conc[i] - conc_copy[i]) /
                            max(fabs(conc[i]), 1e-16));
            conc_copy[i] = conc[i];
        }
        //compare the relative error to the relative tolerance
        if (rel_error < *rel_tol) {
            *max_iterations = i_it;
            break;
        }
    }
    
    //free memory
    free(B_col);
    if (*solveCols == 1) {
        //copy result to init_guess
        memcpy(init_guess, conc, sizeof(double)*nx*ny);
    } else {
        //transpose and copy result to init_guess
        double * ig_ptr = init_guess;
        for (int i_row = 0; i_row < ny; ++i_row) {
            for (int j_col = 0; j_col < nx; ++j_col) {
                *ig_ptr = conc[j_col * ny + i_row];
                ig_ptr++;
            }
        }
        free(conc);
        free(B_org);
    }
    free(conc_copy);
}

/* function for 3D solution */
//a bit long code to avoid if-statements
void solve3D(int *nrow, int *ncol, int *ndepth,
             double *A_mats_horz, double *A_mats_vert,
             double *A_mats_depth, double *B_mat,
             double *conc, int *max_iterations, double *rel_tol) {
    int ny = nrow[0];
    int nx = ncol[0];
    int nz = ndepth[0];
    
    //number of elmements in an A matrix
    int length_mathorz = nx * 3 - 2;
    int length_matvert = ny * 3 - 2;
    int length_matdepth = nz * 3 -2;
    
    //adapt vertical matrix to account for fluxes along x and z axis
    for (int i_slice = 0; i_slice < nz; i_slice++) {
        for (int k_row = 0; k_row < ny; k_row++) {
            for (int j_col = 0; j_col < nx; j_col++) {
                int Pv = (i_slice * nx + j_col) * length_matvert + k_row * 3;
                int Ph = (i_slice * ny + k_row) * length_mathorz + j_col * 3;
                int Pd = (j_col * ny + k_row) * length_matdepth + i_slice * 3;
                A_mats_vert[Pv] = A_mats_vert[Pv] + A_mats_horz[Ph] +
                A_mats_depth[Pd];
            }
        }
    }
    
    //initialize a copy of the conc vector
    double *conc_copy = (double *)malloc(sizeof(double)*nx*ny*nz);
    memcpy(conc_copy, conc, sizeof(double)*nx*ny*nz);
    //vector B needs to be adapted to add info from EW and NS dirs
    double *B_copy = (double *)malloc(sizeof(double) * ny*nx*nz);
    memcpy(B_copy, &B_mat[0], sizeof(double)*ny*nx*nz);
    
    //start to solve 3D system iteratively
    for (int i_it = 0; i_it < *max_iterations; i_it++) {
        R_CheckUserInterrupt();
        
        //counters for iterations
        int i_slice = 0;
        int j_col = 0;
        int startA = 0; //begin of A array for a column in 3D field
        int startB = 0; //idem for B vector of a column
        
        //solve column in the left-front corner
        {
            double *B_col;
            int A_index_horz = 0;
            int A_index_depth = 0;
            int conc_index_horz = 0;
            int conc_index_depth = 0;
            for (int k_row = 0; k_row < ny; k_row++) {
                B_col = &B_copy[startB];
                //--------------------- East Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_horz + ny] * A_mats_horz[A_index_horz +1]; //conc[..+ny] = adjacent column
                //--------------------- North Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_depth + nx * ny] * A_mats_depth[A_index_depth + 1];
                //super efficient loop stuff
                A_index_horz = A_index_horz + length_mathorz;
                A_index_depth = A_index_depth + length_matdepth;
                conc_index_horz = conc_index_horz + 1;
                conc_index_depth = conc_index_depth + 1;
            }
            solveTri( &ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     &B_copy[startB], //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc (same as B_col[0])
        }
        startA = startA + length_matvert;
        startB = startB + ny;
        
        //solve columns from left to right in front slice
        for (j_col = 1; j_col < (nx-1); j_col++) {
            double *B_col;
            int A_index_horz = (i_slice * ny) * length_mathorz + j_col * 3;
            int A_index_vert = (j_col * ny) * length_matdepth + i_slice * 3;
            int conc_index_horz = (i_slice * nx + j_col) * ny;
            int conc_index_vert = (i_slice * nx + j_col) * ny;
            for (int k_row = 0; k_row < ny; k_row++) {
                B_col = &B_copy[startB];
                //--------------------- East Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_horz + ny] *
                A_mats_horz[A_index_horz +1];
                //--------------------- West Flux -------------------------------------
                B_col[k_row] =  B_col[k_row] - conc[conc_index_horz - ny] *
                A_mats_horz[A_index_horz - 1];
                //--------------------- North Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_vert + nx * ny] *
                A_mats_depth[A_index_vert + 1];
                //super efficient loop stuff
                A_index_horz = A_index_horz + length_mathorz;
                A_index_vert = A_index_vert + length_matdepth;
                conc_index_horz = conc_index_horz + 1;
                conc_index_vert = conc_index_vert + 1;
            }
            solveTri( &ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     &B_copy[startB], //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
            startA = startA + length_matvert;
            startB = startB + ny;
        }
        
        
        //solve column on right in front slice
        j_col = nx-1;
        {
            double *B_col;
            int A_index_horz = (i_slice * ny) * length_mathorz +
            j_col * 3;
            int A_index_vert = (j_col * ny) * length_matdepth +
            i_slice * 3;
            int conc_index_horz = (i_slice * nx + j_col) * ny;
            int conc_index_vert = (i_slice * nx + j_col) * ny;
            for (int k_row = 0; k_row < ny; k_row++) {
                B_col = &B_copy[startB];
                //--------------------- West Flux -------------------------------------
                B_col[k_row] =  B_col[k_row] - conc[conc_index_horz - ny] *
                A_mats_horz[A_index_horz - 1];
                //--------------------- North Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_vert + nx * ny] *
                A_mats_depth[A_index_vert + 1];
                //super efficient loop stuff
                A_index_horz = A_index_horz + length_mathorz;
                A_index_vert = A_index_vert + length_matdepth;
                conc_index_horz = conc_index_horz + 1;
                conc_index_vert = conc_index_vert + 1;
            }
            solveTri( &ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     &B_copy[startB], //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
        }
        startA = startA + length_matvert;
        startB = startB + ny;
        
        
        //all slices, except front and back slices
        for (i_slice = 1; i_slice < (nz-1); i_slice++) {
            //left column
            j_col = 0;
            {
                double *B_col;
                int A_index_horz = (i_slice * ny) * length_mathorz +
                j_col * 3;
                int A_index_vert = (j_col * ny) * length_matdepth +
                i_slice * 3;
                int conc_index_horz = (i_slice * nx + j_col) * ny;
                int conc_index_vert = (i_slice * nx + j_col) * ny;
                for (int k_row = 0; k_row < ny; k_row++) {
                    B_col = &B_copy[startB];
                    //--------------------- East Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_horz + ny] *
                    A_mats_horz[A_index_horz +1];
                    //--------------------- North Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_vert + nx * ny] *
                    A_mats_depth[A_index_vert + 1];
                    //--------------------- South Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_vert - nx * ny] *
                    A_mats_depth[A_index_vert - 1];
                    //super efficient loop stuff
                    A_index_horz = A_index_horz + length_mathorz;
                    A_index_vert = A_index_vert + length_matdepth;
                    conc_index_horz = conc_index_horz + 1;
                    conc_index_vert = conc_index_vert + 1;
                }
                solveTri( &ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         &B_copy[startB], //pointer to begin B matrix
                         &conc[startB]); //pointer to begin conc
            }
            startA = startA + length_matvert;
            startB = startB + ny;
            
            //middle columns
            for (j_col = 1; j_col < (nx-1); j_col++) {
                double *B_col;
                int A_index_horz = (i_slice * ny) * length_mathorz +
                j_col * 3;
                int A_index_vert = (j_col * ny) * length_matdepth +
                i_slice * 3;
                int conc_index_horz = (i_slice * nx + j_col) * ny;
                int conc_index_vert = (i_slice * nx + j_col) * ny;
                for (int k_row = 0; k_row < ny; k_row++) {
                    B_col = &B_copy[startB];
                    //--------------------- East Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_horz + ny] *
                    A_mats_horz[A_index_horz +1];
                    //--------------------- West Flux -------------------------------------
                    B_col[k_row] =  B_col[k_row] - conc[conc_index_horz - ny] *
                    A_mats_horz[A_index_horz - 1];
                    //--------------------- North Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_vert + nx * ny] *
                    A_mats_depth[A_index_vert + 1];
                    //--------------------- South Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_vert - nx * ny] *
                    A_mats_depth[A_index_vert - 1];
                    //super efficient loop stuff
                    A_index_horz = A_index_horz + length_mathorz;
                    A_index_vert = A_index_vert + length_matdepth;
                    conc_index_horz = conc_index_horz + 1;
                    conc_index_vert = conc_index_vert + 1;
                }
                solveTri( &ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         &B_copy[startB], //pointer to begin B matrix
                         &conc[startB]); //pointer to begin conc
                startA = startA + length_matvert;
                startB = startB + ny;
            }
            
            //right column
            j_col = nx-1;
            {
                double *B_col;
                int A_index_horz = (i_slice * ny) * length_mathorz +
                j_col * 3;
                int A_index_vert = (j_col * ny) * length_matdepth +
                i_slice * 3;
                int conc_index_horz = (i_slice * nx + j_col) * ny;
                int conc_index_vert = (i_slice * nx + j_col) * ny;
                for (int k_row = 0; k_row < ny; k_row++) {
                    B_col = &B_copy[startB];
                    //--------------------- West Flux -------------------------------------
                    B_col[k_row] =  B_col[k_row] - conc[conc_index_horz - ny] *
                    A_mats_horz[A_index_horz - 1];
                    //--------------------- North Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_vert + nx * ny] *
                    A_mats_depth[A_index_vert + 1];
                    //--------------------- South Flux -------------------------------------
                    B_col[k_row] = B_col[k_row] - conc[conc_index_vert - nx * ny] *
                    A_mats_depth[A_index_vert - 1];
                    //super efficient loop stuff
                    A_index_horz = A_index_horz + length_mathorz;
                    A_index_vert = A_index_vert + length_matdepth;
                    conc_index_horz = conc_index_horz + 1;
                    conc_index_vert = conc_index_vert + 1;
                }
                solveTri(&ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         &B_copy[startB], //pointer to begin B matrix
                         &conc[startB]); //pointer to begin conc
            }
            startA = startA + length_matvert;
            startB = startB + ny;
        }
        
        
        //back slice
        i_slice = nz-1;
        //solve column in the left-front corner
        j_col = 0;
        {
            double *B_col;
            int A_index_horz = (i_slice * ny) * length_mathorz +
            j_col * 3;
            int A_index_vert = (j_col * ny) * length_matdepth +
            i_slice * 3;
            int conc_index_horz = (i_slice * nx + j_col) * ny;
            int conc_index_vert = (i_slice * nx + j_col) * ny;
            for (int k_row = 0; k_row < ny; k_row++) {
                B_col = &B_copy[startB];
                //--------------------- East Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_horz + ny] *
                A_mats_horz[A_index_horz +1];
                //--------------------- South Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_vert - nx * ny] *
                A_mats_depth[A_index_vert - 1];
                //super efficient loop stuff
                A_index_horz = A_index_horz + length_mathorz;
                A_index_vert = A_index_vert + length_matdepth;
                conc_index_horz = conc_index_horz + 1;
                conc_index_vert = conc_index_vert + 1;
            }
            solveTri( &ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     &B_copy[startB], //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
        }
        startA = startA + length_matvert;
        startB = startB + ny;
        
        //solve columns from left to right in back slice
        for (j_col = 1; j_col < (nx-1); j_col++) {
            double *B_col;
            int A_index_horz = (i_slice * ny) * length_mathorz +
            j_col * 3;
            int A_index_vert = (j_col * ny) * length_matdepth +
            i_slice * 3;
            int conc_index_horz = (i_slice * nx + j_col) * ny;
            int conc_index_vert = (i_slice * nx + j_col) * ny;
            for (int k_row = 0; k_row < ny; k_row++) {
                B_col = &B_copy[startB];
                //--------------------- East Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_horz + ny] *
                A_mats_horz[A_index_horz +1];
                //--------------------- West Flux -------------------------------------
                B_col[k_row] =  B_col[k_row] - conc[conc_index_horz - ny] *
                A_mats_horz[A_index_horz - 1];
                //--------------------- South Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_vert - nx * ny] *
                A_mats_depth[A_index_vert - 1];
                //super efficient loop stuff
                A_index_horz = A_index_horz + length_mathorz;
                A_index_vert = A_index_vert + length_matdepth;
                conc_index_horz = conc_index_horz + 1;
                conc_index_vert = conc_index_vert + 1;
            }
            solveTri( &ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     &B_copy[startB], //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
            startA = startA + length_matvert;
            startB = startB + ny;
        }
        
        //solve column on right in back slice
        j_col = nx-1;
        {
            double *B_col;
            int A_index_horz = (i_slice * ny) * length_mathorz +
            j_col * 3;
            int A_index_vert = (j_col * ny) * length_matdepth +
            i_slice * 3;
            int conc_index_horz = (i_slice * nx + j_col) * ny;
            int conc_index_vert = (i_slice * nx + j_col) * ny;
            for (int k_row = 0; k_row < ny; k_row++) {
                B_col = &B_copy[startB];
                //--------------------- West Flux -------------------------------------
                B_col[k_row] =  B_col[k_row] - conc[conc_index_horz - ny] *
                A_mats_horz[A_index_horz - 1];
                //--------------------- South Flux -------------------------------------
                B_col[k_row] = B_col[k_row] - conc[conc_index_vert - nx * ny] *
                A_mats_depth[A_index_vert - 1];
                //super efficient loop stuff
                A_index_horz = A_index_horz + length_mathorz;
                A_index_vert = A_index_vert + length_matdepth;
                conc_index_horz = conc_index_horz + 1;
                conc_index_vert = conc_index_vert + 1;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     &B_copy[startB], //pointer to begin B matrix
                     &conc[startB]); //pointer to begin conc
        }
        
        //evaluate relative error
        int i = 0;
        double rel_error = 0;
        do {
            rel_error = fabs(conc[i] - conc_copy[i]) / max(fabs(conc[i]), 1e-16);
            if (rel_error < *rel_tol) {
                i++;
                continue;
            }
            break; //avoid more if-statements
        } while (i < nx*ny*nz);
        
        //compare the relative error to the relative tolerance
        if (rel_error < *rel_tol) {
            *max_iterations = i_it;
            break;
        } else { //copy concentration for next iteration
            memcpy(conc_copy, conc, sizeof(double)*nx*ny*nz);
            memcpy(B_copy, &B_mat[0], sizeof(double)*ny*nx*nz);
        }
    }
    //free memory
    free(B_copy);
    free(conc_copy);
}

/* function for 3D solution using alternating sweeping direction */
/* it is horribly long, but should be fast */
void solve3D_asd(int *nrow, int *ncol, int *ndepth,
                 double *A_mats_horz, double *A_mats_vert,
                 double *A_mats_depth, double *B_mat,
                 double *conc, int *max_iterations, double *rel_tol) {
    int ny = nrow[0];
    int nx = ncol[0];
    int nz = ndepth[0];
    
    //number of elmements in an A matrix
    int length_mathorz = nx * 3 - 2;
    int length_matvert = ny * 3 - 2;
    int length_matdepth = nz * 3 -2;
    
    //put information of all P cells in the vertical matrix
    for (int i_slice = 0; i_slice < nz; i_slice++) {
        for (int k_row = 0; k_row < ny; k_row++) {
            for (int j_col = 0; j_col < nx; j_col++) {
                int Pv = (i_slice * nx + j_col) * length_matvert +
                k_row * 3;
                int Ph = (i_slice * ny + k_row) * length_mathorz +
                j_col * 3;
                int Pd = (j_col * ny + k_row) * length_matdepth +
                i_slice * 3;
                A_mats_vert[Pv] = A_mats_vert[Pv] + A_mats_horz[Ph] +
                A_mats_depth[Pd];
            }
        }
    }
    
    //a reorganized coefficient matrix
    double * superA = (double *)malloc(sizeof(double) * (4*nx*nz*ny-2*ny*(nx+nz)) );
    double * A_ptr = superA;
    //array to indexes of concentrations
    double ** superC = (double **)malloc(sizeof(double *) * (4*nx*nz*ny-2*ny*(nx+nz)) );
    double ** C_ptr = superC;
    
    int i_slice, k_row, j_col;
    
    //organize information in matrices
    i_slice = 0;
    {
        //column in the left front corner, add E,N fluxes
        for (k_row = 0; k_row < ny; k_row++) {
            //East flux
            *C_ptr = &conc[k_row + ny];
            C_ptr++;
            *A_ptr = A_mats_horz[length_mathorz*k_row +1];
            A_ptr++;
            
            //North flux
            *C_ptr = &conc[k_row + nx*ny];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*k_row +1];
            A_ptr++;
        }
        
        //column 2 of left front to column nx-1, add E,W,N
        for (j_col = 1; j_col < (nx-1); j_col++) {
            for (k_row = 0; k_row < ny; k_row++) {
                //East flux
                *C_ptr = &conc[k_row + (j_col+1) * ny];
                C_ptr++;
                *A_ptr = A_mats_horz[length_mathorz*k_row + j_col*3 +1];
                A_ptr++;
                
                //West flux
                *C_ptr = &conc[k_row + (j_col-1) * ny];
                C_ptr++;
                *A_ptr = A_mats_horz[length_mathorz*k_row + j_col*3 -1];
                A_ptr++;
                
                //North flux
                *C_ptr = &conc[k_row + j_col*ny + nx*ny];
                C_ptr++;
                *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) +1];
                A_ptr++;
            }
        }
        
        //column in the right front corner, add W,N fluxes
        for (k_row = 0; k_row < ny; k_row++) {
            //West flux
            *C_ptr = &conc[k_row + (j_col-1) * ny];
            C_ptr++;
            *A_ptr = A_mats_horz[length_mathorz*k_row + j_col*3 -1];
            A_ptr++;
            
            //North flux
            *C_ptr = &conc[k_row + j_col*ny + nx*ny];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) +1];
            A_ptr++;
        }
    }
    
    for (i_slice = 1; i_slice < nz-1; i_slice++ ) {
        //column on the left, add E,N,S fluxes
        for (k_row = 0; k_row < ny; k_row++) {
            //East flux
            *C_ptr = &conc[i_slice*nx*ny + k_row + ny];
            C_ptr++;
            *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz +1];
            A_ptr++;
            
            //North flux
            *C_ptr = &conc[(i_slice+1)*nx*ny +k_row];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*k_row + 3*i_slice +1];
            A_ptr++;
            
            //South flux
            *C_ptr = &conc[(i_slice-1)*nx*ny + k_row];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*k_row + 3*i_slice -1];
            A_ptr++;
        }
        
        //column in the middle, add E,W,N,S fluxes
        for (j_col = 1; j_col < (nx-1); j_col++) {
            for (k_row = 0; k_row < ny; k_row++) {
                //East flux
                *C_ptr = &conc[i_slice*nx*ny + k_row + (j_col+1) * ny];
                C_ptr++;
                *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz + j_col*3 +1];
                A_ptr++;
                
                //West flux
                *C_ptr = &conc[i_slice*nx*ny + k_row + (j_col-1) * ny];
                C_ptr++;
                *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz + j_col*3 -1];
                A_ptr++;
                
                //North flux
                *C_ptr = &conc[k_row + j_col*ny + (i_slice+1)*nx*ny];
                C_ptr++;
                *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) + 3*i_slice +1];
                A_ptr++;
                
                //South flux
                *C_ptr = &conc[k_row + j_col*ny + (i_slice-1)*nx*ny];
                C_ptr++;
                *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) + 3*i_slice -1];
                A_ptr++;
            }
        }
        
        //column on the right, add W,N,S fluxes
        for (k_row = 0; k_row < ny; k_row++) {
            //West flux
            *C_ptr = &conc[i_slice*nx*ny + k_row + (j_col-1) * ny];
            C_ptr++;
            *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz + j_col*3 -1];
            A_ptr++;
            
            //North flux
            *C_ptr = &conc[k_row + j_col*ny + (i_slice+1)*nx*ny];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) + 3*i_slice +1];
            A_ptr++;
            
            //South flux
            *C_ptr = &conc[k_row + j_col*ny + (i_slice-1)*nx*ny];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) + 3*i_slice -1];
            A_ptr++;
        }
    }
    
    {
        //column in the left back corner, add E,S fluxes
        for (k_row = 0; k_row < ny; k_row++) {
            //East flux
            *C_ptr = &conc[i_slice*nx*ny + k_row + ny];
            C_ptr++;
            *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz +1];
            A_ptr++;
            
            //South flux (j_col = 0, i_slice = nz -1)
            *C_ptr = &conc[k_row + (i_slice-1)*nx*ny];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*k_row + 3*i_slice -1];
            A_ptr++;
        }
        
        //columns in middle back, add E,W,S
        for (j_col = 1; j_col < (nx-1); j_col++) {
            for (k_row = 0; k_row < ny; k_row++) {
                //East flux
                *C_ptr = &conc[i_slice*nx*ny + k_row + (j_col+1) * ny];
                C_ptr++;
                *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz + j_col*3 +1];
                A_ptr++;
                
                //West flux
                *C_ptr = &conc[i_slice*nx*ny + k_row + (j_col-1) * ny];
                C_ptr++;
                *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz + j_col*3 -1];
                A_ptr++;
                
                //South flux
                *C_ptr = &conc[k_row + j_col*ny + (i_slice-1)*nx*ny];
                C_ptr++;
                *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) + 3*i_slice -1];
                A_ptr++;
            }
        }
        
        //column in the back left corner, add W,S fluxes
        for (k_row = 0; k_row < ny; k_row++) {
            //West flux
            *C_ptr = &conc[i_slice*nx*ny + k_row + (j_col-1) * ny];
            C_ptr++;
            *A_ptr = A_mats_horz[(k_row+ny*i_slice)*length_mathorz + j_col*3 -1];
            A_ptr++;
            
            //South flux
            *C_ptr = &conc[k_row + j_col*ny + (i_slice-1)*nx*ny];
            C_ptr++;
            *A_ptr = A_mats_depth[length_matdepth*(k_row + j_col*ny) + 3*i_slice -1];
            A_ptr++;
        }
    }
    
    //**************************************************************
    //***                       solve                            ***
    //**************************************************************
    double B_col[ny];
    int startA, startB;
    
    //initialize a copy of the conc vector to keep track of changes
    double *conc_copy = (double *)malloc(sizeof(double)*nx*ny*nz);
    memcpy(conc_copy, conc, sizeof(double)*nx*ny*nz);
    
    //start to solve 3D system iteratively
    for (int i_it = 0; i_it < max_iterations[0]; i_it++) {
        R_CheckUserInterrupt();
        
        //in theory this could go outside loop to save some femtoseconds
        i_slice = 0; j_col = 0;
        A_ptr = superA;
        C_ptr = superC;
        startA = 0;
        startB = 0;
        
        //front slice
        {
            //front left add E,N fluxes
            for (k_row = 0; k_row < ny; k_row++) {
                B_col[k_row] = B_mat[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = length_matvert;
            startB = ny;
            
            //front, middle columns
            for (j_col = 1; j_col < (nx-1); j_col++) {
                for (k_row = 0; k_row < ny; k_row++) {
                    B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                    B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                    B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                }
                solveTri(&ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]);
                startA = startA + length_matvert;
                startB = startB + ny;
            }
            
            
            //front, right column
            j_col++;
            for (k_row = 0; k_row < ny; k_row++) {
                B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA + length_matvert;
            startB = startB + ny;
        }
        
        //middle slices
        for (i_slice = 1; i_slice < nz-1; i_slice++) {
            //middle, left add E,N,S fluxes
            for (k_row = 0; k_row < ny; k_row++) {
                B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA + length_matvert;
            startB = startB + ny;
            
            //middle, middle add E,W,N,S fluxes
            for (j_col = 1; j_col < (nx-1); j_col++) {
                for (k_row = 0; k_row <ny; k_row++) {
                    B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                    B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                    B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                    B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                }
                solveTri(&ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]);
                startA = startA + length_matvert;
                startB = startB + ny;
            }
            
            
            //middle, right add W,N,S fluxes
            for (k_row = 0; k_row < ny; k_row++) {
                B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA + length_matvert;
            startB = startB + ny;
        }
        
        //back slice
        {
            //back left add E,S fluxes
            for (k_row = 0; k_row < ny; k_row++) {
                B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA + length_matvert;
            startB = startB + ny;
            
            //back, middle columns
            for (j_col = 1; j_col < (nx-1); j_col++) {
                for (k_row = 0; k_row <ny; k_row++) {
                    B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                    B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                    B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr++;
                    C_ptr++;
                }
                solveTri(&ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]);
                startA = startA + length_matvert;
                startB = startB + ny;
            }
            
            //back, right column
            for (k_row = 0; k_row < ny; k_row++) {
                B_col[k_row] = B_mat[startB+k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
                B_col[k_row] = B_col[k_row] - (*A_ptr) * (**C_ptr);
                A_ptr++;
                C_ptr++;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
        }
        
        //reverse direction
        //last slice
        {
            //right back column
            //B_col only contains info from other columns, so should be unchanged
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA - length_matvert;
            
            //back, middle columns
            A_ptr = A_ptr - 2*ny - 1;
            C_ptr = C_ptr - 2*ny - 1;
            for (j_col = 1; j_col < (nx-1); j_col++) {
                for (k_row = 1; k_row < (ny+1); k_row++) {
                    startB--; //once for every new row to get B_mat index
                    B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                    B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                    B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                }
                solveTri(&ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]);
                startA = startA - length_matvert;
            }
            
            //back, left column
            for (k_row = 1; k_row < (ny+1); k_row++) {
                startB--; //once for every new row to get B_mat index
                B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
                B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA - length_matvert;
        }
        
        for (i_slice = 1; i_slice < nz-1; i_slice++) { //goes in opposite direction
            //right middle slice
            for (k_row = 1; k_row < (ny+1); k_row++) {
                startB--; //once for every new row to get B_mat index
                B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
                B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
                B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA - length_matvert;
            
            //middle, middle slices
            for (j_col = 1; j_col < (nx-1); j_col++) { //goes in opposite direction
                for (k_row = 1; k_row < (ny+1); k_row++) {
                    startB--; //once for every new row to get B_mat index
                    B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                    B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                    B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                    B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                }
                solveTri(&ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]);
                startA = startA - length_matvert;
            }
            
            //left middle slice
            for (k_row = 1; k_row < (ny+1); k_row++) {
                startB--; //once for every new row to get B_mat index
                B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
                B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
                B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA - length_matvert;
        }
        
        //first slice to complete the circle of artificial life
        {
            //right front slice
            for (k_row = 1; k_row < (ny+1); k_row++) {
                startB--; //once for every new row to get B_mat index
                B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
                B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA - length_matvert;
            
            //front, middle slices
            for (j_col = 1; j_col < (nx-1); j_col++) { //goes in opposite direction
                for (k_row = 1; k_row < (ny+1); k_row++) {
                    startB--; //once for every new row to get B_mat index
                    B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                    B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                    B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                    A_ptr--;
                    C_ptr--;
                }
                solveTri(&ny, //nin = number of rows square mat
                         &A_mats_vert[startA], //pointer to begin
                         B_col, //pointer to begin B matrix
                         &conc[startB]);
                startA = startA - length_matvert;
            }
            
            //front left slice
            for (k_row = 1; k_row < (ny+1); k_row++) {
                startB--; //once for every new row to get B_mat index
                B_col[ny-k_row] = B_mat[startB] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
                B_col[ny-k_row] = B_col[ny-k_row] - (*A_ptr) * (**C_ptr);
                A_ptr--;
                C_ptr--;
            }
            solveTri(&ny, //nin = number of rows square mat
                     &A_mats_vert[startA], //pointer to begin
                     B_col, //pointer to begin B matrix
                     &conc[startB]);
            startA = startA - length_matvert;
        }
        
        //evaluate relative error
        int i = 0;
        double rel_error;
        do {
            rel_error = fabs(conc[i] - conc_copy[i]) / max(fabs(conc[i]), 1e-16);
            if (rel_error < *rel_tol) {
                i++;
                continue;
            }
            break; //avoid more if-statements
        } while (i < nx*ny*nz);
        
        //compare the relative error to the relative tolerance
        if (rel_error < *rel_tol) {
            *max_iterations = i_it;
            break;
        } else {
            //prepare for next iteration
            //conc_copy is loaded with new concentrations
            memcpy(conc_copy, conc, sizeof(double)*nx*ny*nz);
            rel_error = 0;
        }
    }
    
    //free memory
    free(conc_copy);
    free(superA);
    free(superC);
}

/*
 diff_method = 1 : central differences, 2 : upwind, 3 : hybrid
 bndtype = 1 : fixed flux, 2 : fixed concentration. 3 : no gradient
 bnd_value is dependending on bndtype either flux, concentration or ignored
 boundaries in order horizontal, vertical, depth
 ratecoef is either specified or null
 */
void compose_sscoefmat_1D(int * diff_method,
                          double * D, double * u, double * S,
                          int * nx, int * upbnd_type, double * upbnd_val,
                          int * downbnd_type, double * downbnd_val,
                          double * ratecoef, double * dx, double * coefmat) {
    //adapt reactions for non-uniform geometry
    if (dx != NULL && dx[0] != IGNORE)
        for (int i = 0; i < nx[0]; i++)
            S[i] = S[i] * dx[i];
    
    //use tridiagonal coefmat in condensed form (excluding zeros)
    int length_coefmat = nx[0] * 3 - 2;
    
    //fill middle of matrix
    if (diff_method[0] == 1) { //central differences
        double leftnode_flux, rightnode_flux;
        
        //boundary flux (left side of first node)
        if (upbnd_type[0] == 1) { //fixed flux
            S[0] = S[0] - upbnd_val[0];
            rightnode_flux = 0.0;
        } else if (upbnd_type[0] ==  2) { //fixed concentration
            rightnode_flux = -D[0];
            S[0] = S[0] - (D[0] + u[0]) * upbnd_val[0];
        } else { //no gradient
            rightnode_flux = u[0];
        }
        coefmat[0] = rightnode_flux;
        
        //middle part (exluding first and last cell interfaces)
        for (int i_face = 1; i_face < nx[0]; i_face++) {
            //flux is across an interface with nodes on the left and right
            leftnode_flux = D[i_face] + 0.5 * u[i_face];
            rightnode_flux = -D[i_face] + 0.5 * u[i_face];
            
            //cell indexes of coefficient matrix
            int left_node_p = (i_face - 1) * 3; //index P cell node on the left
            int right_node_p = i_face * 3; //index P cell node on the right
            //flux out on left cell (equivalent to subtractflux in R code)
            //[cell-1][ap] = [cell-1][ap] - left_node_flux
            //[cell-1][ae] = [cell-1][ae] - right_node_flux
            coefmat[left_node_p] = coefmat[left_node_p] - leftnode_flux;
            coefmat[left_node_p+1] = - rightnode_flux;
            //flux in on right cell
            //[cell][ap] = [cell][ap] + left_node_flux
            //[cell][ae] = [cell][ae] + left_node_flux
            coefmat[right_node_p-1] = leftnode_flux;
            coefmat[right_node_p] = rightnode_flux;
        }

        //boundary flux (right side of last node)
        if (downbnd_type[0] == 1) { //fixed flux
            S[nx[0]-1] = S[nx[0]-1] - downbnd_val[0];
            leftnode_flux = 0.0;
        } else if (downbnd_type[0] ==  2) { //fixed concentration
            leftnode_flux = D[nx[0]];
            S[nx[0]-1] = S[nx[0]-1] + (-D[nx[0]] + u[nx[0]]) * downbnd_val[0];
        } else { //no gradient
            leftnode_flux = u[nx[0]];
        }
        coefmat[length_coefmat-1] = coefmat[length_coefmat-1] - leftnode_flux;
    } else if (diff_method[0] == 2) { //upwind differencing scheme
        double leftnode_flux, rightnode_flux;
        
        //boundary flux (left side of first node)
        if (upbnd_type[0] == 1) { //fixed flux
            S[0] = S[0] - upbnd_val[0];
            rightnode_flux = 0.0;
        } else if (upbnd_type[0] ==  2) { //fixed concentration
            rightnode_flux = -D[0] + fmin(0, u[1]);
            S[0] = S[0] - (D[0] + fmax(0, u[0])) * upbnd_val[0];
            
        } else { //no gradient
            rightnode_flux = u[0];
        }
        coefmat[0] = rightnode_flux;
        
        //middle part (exluding first and last cell interfaces)
        for (int i_face = 1; i_face < nx[0]; i_face++) {
            //flux is across an interface with nodes on the left and right
            leftnode_flux = D[i_face] + fmax(0, u[i_face]);
            rightnode_flux = -D[i_face] + fmin(0, u[i_face]);
            //adding/subtracting fluxes
            int left_node_p = (i_face - 1) * 3; //index P cell node on the left
            int right_node_p = i_face * 3; //index P cell node on the right
            coefmat[left_node_p] = coefmat[left_node_p] - leftnode_flux;
            coefmat[left_node_p+1] = - rightnode_flux;
            coefmat[right_node_p-1] = leftnode_flux;
            coefmat[right_node_p] = rightnode_flux;
        }
        
        //boundary flux (right side of last node)
        if (downbnd_type[0] == 1) { //fixed flux
            S[nx[0]-1] = S[nx[0]-1] - downbnd_val[0];
            leftnode_flux = 0.0;
        } else if (downbnd_type[0] ==  2) { //fixed concentration
            leftnode_flux = D[nx[0]] + fmax(0, u[nx[0]]);
            S[nx[0]-1] = S[nx[0]-1] + (-D[nx[0]] + fmin(0, u[nx[0]])) * downbnd_val[0];
        } else { //no gradient
            leftnode_flux = u[nx[0]];
        }
        coefmat[length_coefmat-1] = coefmat[length_coefmat-1] - leftnode_flux;
    } else if (diff_method[0] == 3) { //hybrid differencing scheme
        double leftnode_flux, rightnode_flux;
        
        //boundary flux (left side of first node)
        if (upbnd_type[0] == 1) { //fixed flux
            S[0] = S[0] - upbnd_val[0];
            rightnode_flux = 0.0;
        } else if (upbnd_type[0] ==  2) { //fixed concentration
            rightnode_flux = -D[0] + fmin(0, u[1]);
            S[0] = S[0] - (D[0] + fmax(0, u[0])) * upbnd_val[0];
            
        } else { //no gradient
            rightnode_flux = u[0];
        }
        coefmat[0] = rightnode_flux;
        
        //middle part (exluding first and last cell interfaces)
        for (int i_face = 1; i_face < nx[0]; i_face++) {
            //flux is across an interface with nodes on the left and right
            leftnode_flux = fmax(fmax(0, u[i_face]), D[i_face] + 0.5 * u[i_face]);
            rightnode_flux = fmin(fmin(0, u[i_face]), -D[i_face] + 0.5 * u[i_face]);
            //other stuff
            int left_node_p = (i_face - 1) * 3;
            int right_node_p = i_face * 3;
            coefmat[left_node_p] = coefmat[left_node_p] - leftnode_flux;
            coefmat[left_node_p+1] = - rightnode_flux;
            coefmat[right_node_p-1] = leftnode_flux;
            coefmat[right_node_p] = rightnode_flux;
        }
        
        //boundary flux (right side of last node)
        if (downbnd_type[0] == 1) { //fixed flux
            S[nx[0]-1] = S[nx[0]-1] - downbnd_val[0];
            leftnode_flux = 0.0;
        } else if (downbnd_type[0] ==  2) { //fixed concentration
            leftnode_flux = D[nx[0]] + fmax(0, u[nx[0]]);
            S[nx[0]-1] = S[nx[0]-1] + (-D[nx[0]] + fmin(0, u[nx[0]])) * downbnd_val[0];
        } else { //no gradient
            leftnode_flux = u[nx[0]];
        }
        coefmat[length_coefmat-1] = coefmat[length_coefmat-1] - leftnode_flux;
    }

    
    //add reaction term
    if (ratecoef != NULL && ratecoef[0] != IGNORE) {
        if (dx == NULL || dx[0] == IGNORE)  { //weird, but in steady3d dx is already in ratecoef
          for (int i_node = 0; i_node < nx[0]; i_node++) {
            int node_p = i_node * 3;
            coefmat[node_p] = coefmat[node_p] + ratecoef[i_node];
          }
        } else { //used in calls from R to 1D diect
          for (int i_node = 0; i_node < nx[0]; i_node++) {
            int node_p = i_node * 3;
            coefmat[node_p] = coefmat[node_p] + ratecoef[i_node] * dx[i_node];
          }
        }
    }
}

void steady_to_transient_1D(int * diff_method,
                            double * D, double * u, double * S,
                            int * nx, int * upbnd_type, double * upbnd_val,
                            int * lowbnd_type, double * lowbnd_val,
                            double * ratecoef, double * scale_factor,
                            double * dx, double * coefmat,
                            double * timefrac, double * dt, int * old) {
    //to create vector with 0's use calloc instead of malloc
    double * S_bnd = (double *)calloc(nx[0], sizeof(double));
    
    //don't send reaction rates to function for steady-state matrices
    compose_sscoefmat_1D(diff_method, D, u, S_bnd, nx, upbnd_type,
                         upbnd_val, lowbnd_type, lowbnd_val, ratecoef,
                         dx, coefmat);
    
    double * dt_vec;
    int dt_inc;
    if (scale_factor[0] == IGNORE) {
        dt_inc = 0;
        dt_vec = dt;
    } else {
        dt_inc = 1;
        dt_vec = (double *)malloc(sizeof(double) * nx[0]);
        for (int i_cell = 0; i_cell < nx[0]; ++i_cell) {
            dt_vec[i_cell] = dt[0] / scale_factor[i_cell];
        }
    }
    
    //see for commments the similar 3D function
    double f;
    int sign;
    if (old[0] == 1) {
        f = 1 - timefrac[0];
        sign = 1;
    } else {
        f = timefrac[0];
        sign = -1;
    }
    
    double * A_ptr = &coefmat[0];
    double * dx_ptr = &dx[0];
    double * dt_ptr = &dt_vec[0];
    *A_ptr = *A_ptr * f * (*dt_ptr) * sign + *dx_ptr; //center
    A_ptr++;
    *A_ptr = *A_ptr * f * (*dt_ptr) * sign; //east
    A_ptr++;
    dx_ptr++;
    for (int i = 1; i < nx[0]-1; i++) {
        dt_ptr = dt_ptr + dt_inc;
        *A_ptr = *A_ptr * f * (*dt_ptr) * sign; //west
        A_ptr++;
        *A_ptr = *A_ptr * f * (*dt_ptr) * sign + *dx_ptr; //center
        A_ptr++;
        *A_ptr = *A_ptr * f * (*dt_ptr) * sign; //east
        A_ptr++;
        dx_ptr++;
    }
    dt_ptr = dt_ptr + dt_inc;
    *A_ptr = *A_ptr * f * (*dt_ptr) * sign; //west
    A_ptr++;
    *A_ptr = *A_ptr * f * (*dt_ptr) * sign + *dx_ptr; //center
    
    int addReac = 0;
    if (old[0] == 0 || timefrac[0] == 0) {
        addReac = 1;
    }
    
    dt_ptr = &dt_vec[0];
    for (int i = 0; i < nx[0]; i++) {
        S[i] = *dt_ptr * (S_bnd[i] * f + S[i] * dx[i] * addReac);
        dt_ptr = dt_ptr + dt_inc;
    }
    free(S_bnd);
    if (scale_factor[0] != IGNORE)
        free(dt_vec);
}

/*
 diff_method = 1 : central differences, 2 : upwind, 3 : hybrid
 bndtype = 1 : fixed flux, 2 : fixed concentration. 3 : no gradient
 bnd_value is dependending on bndtype either flux, concentration or ignored
 ratecoef is either specified or -1
 bnd_type, bnd_value = specification boundary conditions,
 singleBbnd is a boolean whether a single value is used for the entire face
 n is a vectors with lenght = 3 for x,y,z direction
 Dx, ux are filled per row, Dy per column etc
 multByArea: whether the D and u arrays should be multiplied by areas (should be true first time)
 S is filled by column from left to right and then front to back
 
 WARNING: function is destructive to D and u arrays
 */
void compose_sscoefmat_2D(int * diff_method,
                          double * Dx, double * ux, //diffcoefs & adv x-direction
                          double * Dy, double * uy, //...  y-direction
                          int * multByArea,
                          double * reac,
                          double * ratecoef,
                          int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                          int * westbnd_type, double * westbnd_val, int * singleBndWest,
                          int * upbnd_type, double * upbnd_val, int * singleBndUp,
                          int * downbnd_type, double * downbnd_val, int * singleBndDown,
                          int * n, double * dx, double * dy,
                          double * A_mats_horz, double * A_mats_vert, double * B) {
    int nx = n[0];
    int ny = n[1];

    //when this function is called by steady_to_transient2d it is NULL
    if (reac != NULL) {
        double * reac_ptr = reac;
        double * B_ptr = B;
        for (int j_col = 0; j_col < nx; j_col++) {
            for (int k_row = 0; k_row < ny; k_row++) {
                *B_ptr = *reac_ptr * dx[j_col] * dy[k_row];
                reac_ptr++;
                B_ptr++;
            }
        }
    }
    
    //when this function is called by steady_to_transient2d it can be specified
    if (ratecoef != NULL && ratecoef[0] != IGNORE) {
        double * k_ptr = ratecoef;
        for (int j_col = 0; j_col < nx; j_col++) {
            for (int k_row = 0; k_row < ny; k_row++) {
                *k_ptr = *k_ptr * dx[j_col] * dy[k_row];
                k_ptr++;
            }
        }
    }
    
    //when the boundary conditions are fluxes they need to be multiplied with the area
    //the boundary fluxes can change per column or row
    for (int i_row = 0; i_row < ((ny - 1) * (1 - *singleBndEast) + 1); i_row++) {
        if (eastbnd_type[i_row] == 1) //Neumann: a specified flux
            eastbnd_val[i_row] = eastbnd_val[i_row] * dy[i_row]; //dz = 1
    }
    for (int i_row = 0; i_row < ((ny - 1) * (1 - *singleBndWest) + 1); i_row++) {
        if (westbnd_type[i_row] == 1)
            westbnd_val[i_row] = westbnd_val[i_row] * dy[i_row];
    }
    for (int j_col = 0; j_col < ((nx - 1) * (1 - *singleBndUp) + 1); j_col++) {
        if (upbnd_type[j_col] == 1)
            upbnd_val[j_col] = upbnd_val[j_col] * dx[j_col];
    }
    for (int j_col = 0; j_col < ((nx - 1) * (1 - *singleBndDown) + 1); j_col++) {
        if (downbnd_type[j_col] == 1)
            downbnd_val[j_col] = downbnd_val[j_col] * dx[j_col];
    }
    
    
    int size_A_row = nx * 3 -2;
    int size_A_col = ny * 3 -2;
    
    //Multiply D and u arrays by area, which is only necesarry for first call with
    // new D and u arrays, also change the source term
    if (multByArea[0] == 1) {
        double * D_ptr = Dy;
        double * u_ptr = uy;
        for (int j_col = 0; j_col < nx; j_col++) {
            for (int l = 0;  l < (ny+1); l++) {
                *D_ptr = *D_ptr * dx[j_col];
                D_ptr++;
                *u_ptr = *u_ptr * dx[j_col];
                u_ptr++;
            }
        }

        D_ptr = Dx;
        u_ptr = ux;
        for (int j_row = 0; j_row < ny; j_row++) {
            for (int l = 0; l < (nx+1); l++) {
                *D_ptr = *D_ptr * dy[j_row];
                D_ptr++;
                *u_ptr = *u_ptr * dy[j_row];
                u_ptr++;
            }
        }
    }
    
    int startA = 0;
    int first_bnd_index = 0, second_bnd_index = 0;
    
    //make matrices for all columns
    for (int j_col = 0; j_col < nx; j_col++) {
        int startTrans = j_col * (ny+1);
        int startS = j_col * ny;
            
        //make the coeficent matrix (in A_mats_vert) and boundary conditions (in Sy)
        double * k_ptr = NULL;
        if (ratecoef != NULL && ratecoef[0] != IGNORE) {
            k_ptr = &ratecoef[ny * j_col];
        }
        
        compose_sscoefmat_1D(diff_method,
                             &Dy[startTrans], &uy[startTrans], &B[startS], &ny,
                             &upbnd_type[first_bnd_index], &upbnd_val[first_bnd_index],
                             &downbnd_type[second_bnd_index],
                             &downbnd_val[second_bnd_index],
                             k_ptr, NULL, &A_mats_vert[startA]);
        first_bnd_index = first_bnd_index + (1 - *singleBndUp);
        second_bnd_index = second_bnd_index + (1 - *singleBndDown);
        startA = startA + size_A_col;
    }
    
    startA = 0;
    first_bnd_index = 0, second_bnd_index = 0;
    //make matrices for all rows
    double * Sx = (double *)malloc(sizeof(double)*nx);
    for (int k = 0; k < nx; k++) {
        Sx[k] = 0;
    }
    
    for (int j_row = 0; j_row < ny; j_row++) {
        int startTrans = j_row * (nx+1);
            
        //make the coeficent matrix (in A_mats_horz) and boundary conditions (in Sx)
        compose_sscoefmat_1D(diff_method,
                             &Dx[startTrans], &ux[startTrans], Sx, &nx,
                             &westbnd_type[first_bnd_index], &westbnd_val[first_bnd_index],
                             &eastbnd_type[second_bnd_index],
                             &eastbnd_val[second_bnd_index],
                             NULL, NULL, &A_mats_horz[startA]);
            
        //copy boundary conditions to src vector
        for (int k = 0; k < nx; k++) {
            int index = j_row + k * ny;
            B[index] = B[index] + Sx[k];
            Sx[k] = 0;
        }
        first_bnd_index = first_bnd_index + (1 - *singleBndWest);
        second_bnd_index = second_bnd_index + (1 - *singleBndEast);
        startA = startA + size_A_row;
    }
    free(Sx);
}

//create a transient matrix for transport, do not include reactions
void steady_to_transient_2D(int * diff_method,
                            double * Dx, double * ux, //diffcoefs & adv x-direction
                            double * Dy, double * uy, //...  y-direction
                            double * reac,
                            double * ratecoef,
                            double * scale_factor,
                            int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                            int * westbnd_type, double * westbnd_val, int * singleBndWest,
                            int * upbnd_type, double * upbnd_val, int * singleBndUp,
                            int * downbnd_type, double * downbnd_val, int * singleBndDown,
                            int * n, double * dx, double * dy,
                            double * A_mats_horz, double * A_mats_vert,
                            double * B,
                            double * timefrac, double * dt, int * old) {
    int nx = n[0];
    int ny = n[1];
    
    //set parameters to transform steady-state into transient-state
    double f; //used to specify either explicit, semi-implicit, or implicit
    int sign; //sign is dependent on new or old state matrices, as 1 or -1
    if (old[0] == 1) {
        f = 1 - timefrac[0];
        sign = 1;
    } else {
        f = timefrac[0];
        sign = -1;
    }
    
    //make 2D steady-state coefficient matrix and source array (B) for boundary conditions (reactions are set to NULL)
    int multbyarea = 0;
    if (*old == 1) multbyarea = 1;
    if (*timefrac == 1) multbyarea = 1;
    
    //account for the scale factor through time
    double * dt_vec;
    int dt_inc;
    if (scale_factor[0] == IGNORE) {
        dt_inc = 0;
        dt_vec = dt;
    } else {
        dt_inc = 1;
        dt_vec = (double *)malloc(sizeof(double)*nx*ny);
        for (int i_cell = 0; i_cell < nx*ny; i_cell++) {
            dt_vec[i_cell] = dt[0] / scale_factor[i_cell];
        }
    }
    
    compose_sscoefmat_2D(diff_method,
                         Dx, ux, //diffcoefs & adv x-direction
                         Dy, uy, //...  y-direction
                         &multbyarea, //if old=true or timefrac = 1, multiply by area
                         NULL, ratecoef,
                         eastbnd_type, eastbnd_val, singleBndEast,
                         westbnd_type, westbnd_val, singleBndWest,
                         upbnd_type, upbnd_val, singleBndUp,
                         downbnd_type, downbnd_val, singleBndDown,
                         n, dx, dy,
                         A_mats_horz, A_mats_vert, B);
    
    //change all horizontal matrices
    double * A_ptr = A_mats_horz;
    double * dt_ptr = dt_vec;
    double mult = f * sign;
    for (int i_row = 0; i_row < ny; i_row++) {
        dt_ptr = &dt_vec[dt_inc * i_row];
        double * dx_ptr = &dx[0]; //reset pointer to first cell of dx vector
        *A_ptr = *A_ptr * mult * (*dt_ptr) + dx_ptr[0] * dy[i_row]; //P cell
        A_ptr++;
        dx_ptr++;
        *A_ptr = *A_ptr * mult * (*dt_ptr); //east
        A_ptr++;
        for (int j_col = 1; j_col < (nx-1); j_col++) {
            dt_ptr = &dt_vec[dt_inc * (i_row + j_col * ny)];
            *A_ptr = *A_ptr * mult * (*dt_ptr); //west
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr) + dx_ptr[0] * dy[i_row]; //P cell
            A_ptr++;
            dx_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //east
            A_ptr++;
            dt_ptr = dt_ptr + dt_inc;
        }
        dt_ptr = &dt_vec[dt_inc * (i_row + (nx-1) * ny)];
        *A_ptr = *A_ptr * mult * (*dt_ptr); //west
        A_ptr++;
        *A_ptr = *A_ptr * mult * (*dt_ptr) + dx_ptr[0] * dy[i_row]; //P cell
        A_ptr++;
    }
    
    //change all vertical matrices
    A_ptr = A_mats_vert;
    dt_ptr = dt_vec;
    for (int j_col = 0; j_col < nx; ++j_col) {
        *A_ptr = *A_ptr * mult * (*dt_ptr); //P
        A_ptr++;
        *A_ptr = *A_ptr * mult * (*dt_ptr); //E
        A_ptr++;
        dt_ptr = dt_ptr + dt_inc;
        for (int i_row = 1; i_row < (ny-1); ++i_row) {
            *A_ptr = *A_ptr * mult * (*dt_ptr); //W
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //P
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //E
            A_ptr++;
            dt_ptr = dt_ptr + dt_inc;
        }
        *A_ptr = *A_ptr * mult * (*dt_ptr); //W
        A_ptr++;
        *A_ptr = *A_ptr * mult * (*dt_ptr); //P
        A_ptr++;
        dt_ptr = dt_ptr + dt_inc;
    }
    
    mult = f;
    dt_ptr = dt_vec;
    double * B_ptr = B;
    double * reac_ptr = reac;
    for (int j_col = 0; j_col < nx; j_col++) { //order cols and rows matters
        for (int i_row = 0; i_row < ny; i_row++) {
            *B_ptr = (*B_ptr + *reac_ptr * dx[j_col] * dy[i_row]) * mult * (*dt_ptr);
            B_ptr++;
            reac_ptr++;
            dt_ptr = dt_ptr + dt_inc;
        }
    }
    
    if (scale_factor[0] != IGNORE)
        free(dt_vec);
}

//timefrac: 0 = explicit, 1 < timefrac < 0 = semi-implicit, 1 = implicity
//dt is timestep, init is old concentrations organized in fist columns and then slices
//new_conc is only used for explicit method, when solution can be obtained without..
// ... matrix inversion.
//for other variables see description compose_sscoefmat_2D
void compose_transcoefmat_2D(int * diff_method,
                             double * Dx, double * ux, //diffcoefs & adv x-direction
                             double * Dy, double * uy, //...  y-direction
                             double * reac,
                             double * ratecoef,
                             double * scale_factor,
                             int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                             int * westbnd_type, double * westbnd_val, int * singleBndWest,
                             int * upbnd_type, double * upbnd_val, int * singleBndUp,
                             int * downbnd_type, double * downbnd_val, int * singleBndDown,
                             int * n, double * dx, double * dy,
                             double * A_mats_horz, double * A_mats_vert,
                             double * B,
                             double * timefrac, double * dt, double * init) {
    int nx = n[0];
    int ny = n[1];
    
    //obtain coefficient matrixes for old state variables
    int old = 1;
    double * rhs = (double *)malloc(nx*ny*sizeof(double));
    
    if (timefrac[0] != 1) {
        steady_to_transient_2D(diff_method,
                               Dx, ux, //diffcoefs & adv x-direction
                               Dy, uy, //...  y-direction
                               reac,
                               ratecoef,
                               scale_factor,
                               eastbnd_type, eastbnd_val, singleBndEast,
                               westbnd_type, westbnd_val, singleBndWest,
                               upbnd_type, upbnd_val, singleBndUp,
                               downbnd_type, downbnd_val, singleBndDown,
                               n, dx, dy,
                               A_mats_horz, A_mats_vert, B,
                               timefrac, dt, &old);
        
        //A * new_time_statevars - S_new = B * previous_time_statevars - S_old
        //Solve right-hand side
        //also add time derivative (+diag(1, ny)) to vertical matrices
        //first info from all vertical matrices
        int rhs_index = 0;
        int ap_vert = 0;
        for (int j_col = 0; j_col < nx; j_col++) {
            rhs[rhs_index] = -B[rhs_index] +
                A_mats_vert[ap_vert] * init[rhs_index] + //vertical ap * Cp
                A_mats_vert[ap_vert+1] * init[rhs_index+1]; //vertical as * Cs (cell below)
            rhs_index++;
            ap_vert = ap_vert + 3;
            for (int k_row = 1; k_row < (ny-1); k_row++) {
                rhs[rhs_index] = -B[rhs_index] + A_mats_vert[ap_vert-1] * init[rhs_index-1] + //vertical an * Cn (cell above)
                    A_mats_vert[ap_vert] * init[rhs_index] + //vertical ap * Cp
                    A_mats_vert[ap_vert+1] * init[rhs_index+1]; //vertical as * Cs (cell below)
                rhs_index++;
                ap_vert = ap_vert + 3;
            }
            rhs[rhs_index] = -B[rhs_index] +
                A_mats_vert[ap_vert-1] * init[rhs_index-1] + //vertical an * Cn (cell above)
                A_mats_vert[ap_vert] * init[rhs_index]; //vertical ap * Cp
            rhs_index++;
            ap_vert = ap_vert + 1;
        }
        
        //info from all horizontal matrices
        int ap_horz = 0;
        int rhs_w, rhs_p, rhs_e;
        for (int k_row = 0; k_row < ny; k_row++) {
            rhs_p = k_row;
            rhs_e = rhs_p + ny;
            rhs[rhs_p] = rhs[rhs_p] +
                A_mats_horz[ap_horz] * init[rhs_p] +
                A_mats_horz[ap_horz+1] * init[rhs_e]; //east cell
            ap_horz = ap_horz + 3;
            for (int j_col = 1; j_col < (nx-1); j_col++) {
                rhs_w = rhs_p;
                rhs_p = rhs_p + ny;
                rhs_e = rhs_p + ny;
                rhs[rhs_p] = rhs[rhs_p] +
                    A_mats_horz[ap_horz-1] * init[rhs_w] + //west cell
                    A_mats_horz[ap_horz] * init[rhs_p] +
                    A_mats_horz[ap_horz+1] * init[rhs_e]; //east cell
                ap_horz = ap_horz + 3;
            }
            rhs_w = rhs_p;
            rhs_p = rhs_p + ny;
            rhs[rhs_p] = rhs[rhs_p] +
                A_mats_horz[ap_horz-1] * init[rhs_w] + //west cell
                A_mats_horz[ap_horz] * init[rhs_p];
            ap_horz = ap_horz + 1;
        }
    } else { //for implicit rhs is simply C * volume
        double * rhs_ptr = &rhs[0];
        double * init_ptr = &init[0];
        for (int j_col = 0; j_col < nx; j_col++) {
            double areamult = dx[j_col];
            for (int k_row = 0; k_row < ny; k_row++) {
                rhs_ptr[0] = init_ptr[0] * areamult * dy[k_row];
                rhs_ptr++;
                init_ptr++;
            }
        }
    }
    
    if (timefrac[0] == 0) { //explicit
        //solution depends only on old concentrations, i.e. lhs would be identity matrix * volume
        double * new_conc_ptr = &init[0];
        double * rhs_ptr = &rhs[0];
        for (int j_col = 0; j_col < nx; j_col++) {
            double areadiv = dx[j_col];
            for (int k_row = 0; k_row < ny; k_row++) {
                *new_conc_ptr = *rhs_ptr / (dy[k_row] * areadiv);
                new_conc_ptr++;
                rhs_ptr++;
            }
        }
    } else { //semi-implicit or implicit
        //solution cannot be obtained without matrix inversion
        old = 0; //FALSE for new matrices
        
        //obtain matrices for new concentrations, left-hand side
        steady_to_transient_2D(diff_method,
                               Dx, ux, //diffcoefs & adv x-direction
                               Dy, uy, //...  y-direction
                               reac,
                               ratecoef,
                               scale_factor,
                               eastbnd_type, eastbnd_val, singleBndEast,
                               westbnd_type, westbnd_val, singleBndWest,
                               upbnd_type, upbnd_val, singleBndUp,
                               downbnd_type, downbnd_val, singleBndDown,
                               n, dx, dy,
                               A_mats_horz, A_mats_vert,
                               B, timefrac, dt, &old);
        //combine source term new matrices with rhs matrix
        for (int i = 0; i < nx*ny; i++) {
            B[i] = -B[i] + rhs[i];
        }
    }
    free(rhs);
}

/*
 diff_method = 1 : central differences, 2 : upwind, 3 : hybrid
 bndtype = 1 : fixed flux, 2 : fixed concentration. 3 : no gradient
 bnd_value is dependending on bndtype either flux, concentration or ignored
 ratecoef is either specified or -1
 bnd_type, bnd_value = specification boundary conditions, 
 singleBbnd is a boolean whether a single value is used for the entire face 
 n is a vectors with lenght = 3 for x,y,z direction
 Dx, ux are filled per row, Dy per column etc
 multByArea: whether the D and u arrays should be multiplied by areas (should be true first time)
 S is filled by column from left to right and then front to back

 WARNING: function is destructive to D and u arrays
 */
void compose_sscoefmat_3D(int * diff_method,
                          double * Dx, double * ux, //diffcoefs & adv x-direction
                          double * Dy, double * uy, //...  y-direction
                          double * Dz, double * uz, //... z-direc
                          int * multByArea,
                          double * reac,
                          double * ratecoef,
                          int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                          int * westbnd_type, double * westbnd_val, int * singleBndWest,
                          int * upbnd_type, double * upbnd_val, int * singleBndUp,
                          int * downbnd_type, double * downbnd_val, int * singleBndDown,
                          int * northbnd_type, double * northbnd_val, int * singleBndNorth,
                          int * southbnd_type, double * southbnd_val, int * singleBndSouth,
                          int * n, double * dx, double * dy, double * dz,
                          double * A_mats_horz, double * A_mats_vert,
                          double * A_mats_depth, double * B) {
    int nx = n[0];
    int ny = n[1];
    int nz = n[2];

    //when this function is called by steady_to_transient3d it is NULL
    if (reac != NULL) {
        double * reac_ptr = reac;
        double * B_ptr = B;
        for (int i_slice = 0; i_slice < nz; i_slice++) {
          for (int j_col = 0; j_col < nx; j_col++) {
            double area = dz[i_slice] * dx[j_col];
            for (int k_row = 0; k_row < ny; k_row++) {
              *B_ptr = *reac_ptr * area * dy[k_row]; //multiply by volume
              reac_ptr++;
              B_ptr++;
            }
          }
        }
    }

    if (ratecoef != NULL && ratecoef[0] != IGNORE) {
      double * k_ptr = ratecoef; 
      for (int i_slice = 0; i_slice < nz; i_slice++) {
        for (int j_col = 0; j_col < nx; j_col++) {
          double area = dz[i_slice] * dx[j_col];
          for (int k_row = 0; k_row < ny; k_row++) {
            *k_ptr = *k_ptr * area * dy[k_row]; //multiply by volume
            k_ptr++;
          }
        }
      }
    }
    

    int size_A_row = nx * 3 -2;
    int size_A_col = ny * 3 -2;
    int size_A_depth = nz * 3 -2;
    
    //Multiply D and u arrays by area, which is only necesarry for first call with
    // new D and u arrays, also change the source term
    if (multByArea[0] == 1) {
        double * D_ptr = &Dy[0];
        double * u_ptr = &uy[0];
        for (int i_slice = 0; i_slice < nz; i_slice++) {
            for (int j_col = 0; j_col < nx; j_col++) {
                double area = dx[j_col] * dz[i_slice];
                for (int l = 0;  l < (ny+1); l++) {
                  *D_ptr = *D_ptr * area; 
                  D_ptr++;
                  *u_ptr = *u_ptr * area;
                  u_ptr++;
                }
            }
        }
        D_ptr = &Dx[0];
        u_ptr = &ux[0];
        for (int i_slice = 0; i_slice < nz; i_slice++) {
            for (int j_row = 0; j_row < ny; j_row++) {
                double area = dy[j_row] * dz[i_slice];
                for (int l = 0; l < (nx+1); l++) {
                  *D_ptr = *D_ptr * area; 
                  D_ptr++;
                  *u_ptr = *u_ptr * area;
                  u_ptr++;
                }
            }
        }
        D_ptr = &Dz[0];
        u_ptr = &uz[0];
        for (int i_col = 0; i_col < nx; i_col++) {
            for (int j_row = 0; j_row < ny; j_row++) {
                double area = dx[i_col] * dy[j_row];
                for (int l = 0; l < (nz+1); l++) {
                  *D_ptr = *D_ptr * area; 
                  D_ptr++;
                  *u_ptr = *u_ptr * area;
                  u_ptr++;
                }
            }
        }
    }
    
    int startA = 0;
    int first_bnd_index = 0, second_bnd_index = 0;
    //make matrices for all columns
    for (int i_slice = 0; i_slice < nz; i_slice++) {
        for (int j_col = 0; j_col < nx; j_col++) {
            int startTrans = (i_slice * nx + j_col) * (ny+1);
            int startS = (i_slice * nx + j_col) * ny;
            
            //make the coeficent matrix (in A_mats_vert) and boundary conditions (in Sy)
            double * k_ptr = NULL;
            if (ratecoef != NULL && ratecoef[0] != IGNORE) {
              k_ptr = &ratecoef[ny * (j_col + i_slice * nx)];
            }
            compose_sscoefmat_1D(diff_method,
                                 &Dy[startTrans], &uy[startTrans], &B[startS], &ny,
                                 &upbnd_type[first_bnd_index], &upbnd_val[first_bnd_index],
                                 &downbnd_type[second_bnd_index], &downbnd_val[second_bnd_index],
                                 k_ptr, NULL, &A_mats_vert[startA]);
            first_bnd_index = first_bnd_index + (1 - *singleBndUp);
            second_bnd_index = second_bnd_index + (1 - *singleBndDown);
            startA = startA + size_A_col;
        }
    }
    
    startA = 0;
    first_bnd_index = 0, second_bnd_index = 0;
    //make matrices for all rows
    double * Sx = (double *)malloc(sizeof(double)*nx);
    for (int k = 0; k < nx; k++) {
      Sx[k] = 0;
    }
    for (int i_slice = 0; i_slice < nz; i_slice++) {
        for (int j_row = 0; j_row < ny; j_row++) {
            int startTrans = (i_slice * ny + j_row) * (nx+1);
            
            //make the coeficent matrix (in A_mats_horz) and boundary conditions (in Sx)
            compose_sscoefmat_1D(diff_method,
                                 &Dx[startTrans], &ux[startTrans], Sx, &nx,
                                 &westbnd_type[first_bnd_index], &westbnd_val[first_bnd_index],
                                 &eastbnd_type[second_bnd_index], &eastbnd_val[second_bnd_index],
                                 NULL, NULL, &A_mats_horz[startA]);
    
            //copy boundary conditions to src vector
            for (int k = 0; k < nx; k++) {
                int index = j_row + (k + i_slice * nx) * ny;
                B[index] = B[index] + Sx[k];
                Sx[k] = 0;
            }
            first_bnd_index = first_bnd_index + (1 - *singleBndWest);
            second_bnd_index = second_bnd_index + (1 - *singleBndEast);
            startA = startA + size_A_row;
        }
    }
    free(Sx);
 
    startA = 0;
    first_bnd_index = 0, second_bnd_index = 0;
    //make matrices for all rows in depth (front-back direction)
    double * Sz = (double *)malloc(sizeof(double)*nz);
    for (int k = 0; k < nz; k++) {
      Sz[k] = 0;
    }
    for (int i_col = 0; i_col < nx; i_col++) {
        for (int j_row = 0; j_row < ny; j_row++) {
            int startTrans = (i_col * ny + j_row) * (nz+1);

            //make the coeficent matrix (in A_mats_horz) and boundary conditions (in Sz)
            compose_sscoefmat_1D(diff_method,
                                 &Dz[startTrans], &uz[startTrans], Sz, &nz, 
                                 &northbnd_type[first_bnd_index], &northbnd_val[first_bnd_index],
                                 &southbnd_type[second_bnd_index], &southbnd_val[second_bnd_index],
                                 NULL, NULL, &A_mats_depth[startA]);
            //copy boundary conditions to src vector
            for (int k = 0; k < nz; k++) {
                int index = j_row + (i_col + k * nx) * ny;
                B[index] = B[index] + Sz[k];
                Sz[k] = 0;
            }
            first_bnd_index = first_bnd_index + (1 - *singleBndNorth);
            second_bnd_index = second_bnd_index + (1 - *singleBndSouth);
            startA = startA + size_A_depth;
        }
    }
    free(Sz);
}

//create a transient matrix for transport, do not include reactions
void steady_to_transient_3D(int * diff_method,
                            double * Dx, double * ux, //diffcoefs & adv x-direction
                            double * Dy, double * uy, //...  y-direction
                            double * Dz, double * uz, //... z-direc
                            double * reac,
                            double * ratecoef,
                            double * scale_factor,
                            int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                            int * westbnd_type, double * westbnd_val, int * singleBndWest,
                            int * upbnd_type, double * upbnd_val, int * singleBndUp,
                            int * downbnd_type, double * downbnd_val, int * singleBndDown,
                            int * northbnd_type, double * northbnd_val, int * singleBndNorth,
                            int * southbnd_type, double * southbnd_val, int * singleBndSouth,
                            int * n, double * dx, double * dy, double * dz,
                            double * A_mats_horz, double * A_mats_vert,
                            double * A_mats_depth, double * B,
                            double * timefrac, double * dt, int * old) {
    int nx = n[0];
    int ny = n[1];
    int nz = n[2];
    
    //set parameters to transform steady-state into transient-state
    double f; //used to specify either explicit, semi-implicit, or implicit
    int sign; //sign is dependent on new or old state matrices, as 1 or -1
    if (old[0] == 1) {
        f = 1 - timefrac[0];
        sign = 1;
    } else {
        f = timefrac[0];
        sign = -1;
    }
    
    int multbyarea = 0;
    if (*old == 1) multbyarea = 1;
    if (*timefrac == 1) multbyarea = 1;
    
    //account for the scale factor through time
    double * dt_vec;
    int dt_inc;
    if (scale_factor[0] == IGNORE) {
        dt_inc = 0;
        dt_vec = dt;
    } else {
        dt_inc = 1;
        dt_vec = (double *)malloc(sizeof(double)*nx*ny*nz);
        for (int i_cell = 0; i_cell < nx*ny*nz; i_cell++) {
            dt_vec[i_cell] = dt[0] / scale_factor[i_cell];
        }
    }
    
    //make 3D steady-state coefficient matrix and source array (B) for boundary conditions
    compose_sscoefmat_3D(diff_method,
                         Dx, ux, //diffcoefs & adv x-direction
                         Dy, uy, //...  y-direction
                         Dz, uz, //... z-direc
                         &multbyarea,
                         NULL, ratecoef,
                         eastbnd_type, eastbnd_val, singleBndEast,
                         westbnd_type, westbnd_val, singleBndWest,
                         upbnd_type, upbnd_val, singleBndUp,
                         downbnd_type, downbnd_val, singleBndDown,
                         northbnd_type, northbnd_val, singleBndNorth,
                         southbnd_type, southbnd_val, singleBndSouth,
                         n, dx, dy, dz,
                         A_mats_horz, A_mats_vert,
                         A_mats_depth, B);
    
    //horizontal matrices are multiplied by mult and time derivative is added
    double * A_ptr = A_mats_horz;
    double mult = f * sign;
    double * dt_ptr;
    for (int j_slice = 0; j_slice < nz; j_slice++) {
        for (int i_row = 0; i_row < ny; i_row++) {
            dt_ptr = &dt_vec[dt_inc * (i_row + j_slice*nx*ny)];
            double * dx_ptr = &dx[0]; //reset pointer to first cell of dx vector
            double area = dy[i_row] * dz[j_slice];
            *A_ptr = *A_ptr * mult * (*dt_ptr) + dx_ptr[0] * area; //P cell
            A_ptr++;
            dx_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //east
            A_ptr++;
            for (int j_col = 1; j_col < (nx-1); j_col++) {
                dt_ptr = &dt_vec[dt_inc * (i_row + (j_col + j_slice * nx) * ny)];
                *A_ptr = *A_ptr * mult * (*dt_ptr); //west
                A_ptr++;
                *A_ptr = *A_ptr * mult * (*dt_ptr) + dx_ptr[0] * area; //P cell
                A_ptr++;
                dx_ptr++;
                *A_ptr = *A_ptr * mult * (*dt_ptr); //east
                A_ptr++;
            }
            dt_ptr = &dt_vec[dt_inc * (i_row + ( (nx-1) + j_slice * nx) * ny)];
            *A_ptr = *A_ptr * mult * (*dt_ptr); //west
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr) + dx_ptr[0] * area; //P cell
            A_ptr++;
        }
    }
    
    //vertical matrices are multiplied by mult
    dt_ptr = dt_vec;
    A_ptr = A_mats_vert;
    for (int i_slice = 0; i_slice < nz; i_slice++) {
        for (int j_col = 0; j_col < nx; j_col++) {
            *A_ptr = *A_ptr * mult * (*dt_ptr); //P
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //E
            A_ptr++;
            dt_ptr = dt_ptr + dt_inc;
            for (int k_row = 1; k_row < (ny-1); k_row++) {
                *A_ptr = *A_ptr * mult * (*dt_ptr); //W
                A_ptr++;
                *A_ptr = *A_ptr * mult * (*dt_ptr); //P
                A_ptr++;
                *A_ptr = *A_ptr * mult * (*dt_ptr); //E
                A_ptr++;
                dt_ptr = dt_ptr + dt_inc;
            }
            *A_ptr = *A_ptr * mult * (*dt_ptr); //W
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //P
            A_ptr++;
            dt_ptr = dt_ptr + dt_inc;
        }
    }
    
    //depth matrices are multiplied by mult
    A_ptr = A_mats_depth;
    for (int j_col = 0; j_col < nx; j_col++) {
        for (int k_row = 0; k_row < ny; k_row++) {
            dt_ptr = &dt_vec[dt_inc * (k_row + j_col * ny)];
            *A_ptr = *A_ptr * mult * (*dt_ptr); //P
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //E
            A_ptr++;
            for (int i_slice = 1; i_slice < (nz-1); i_slice++) {
                dt_ptr = &dt_vec[dt_inc * ( k_row + (j_col + i_slice * nx) * ny )];
                *A_ptr = *A_ptr * mult * (*dt_ptr); //W
                A_ptr++;
                *A_ptr = *A_ptr * mult * (*dt_ptr); //P
                A_ptr++;
                *A_ptr = *A_ptr * mult * (*dt_ptr); //E
                A_ptr++;
            }
            dt_ptr = &dt_vec[dt_inc * (k_row + (j_col + (nz-1) * nx) * ny)];
            *A_ptr = *A_ptr * mult * (*dt_ptr); //W
            A_ptr++;
            *A_ptr = *A_ptr * mult * (*dt_ptr); //P
            A_ptr++;
        }
    }
    
    //adapt boundary conditions and reactions
    //f makes sure that reactions are only added once
    mult = f * dt[0];
    double * reac_ptr = reac;
    double * B_ptr = B;
    for (int i_slice = 0; i_slice < nz; i_slice++)
        for (int j_col = 0; j_col < nx; j_col++) {
            double area = dx[j_col] * dz[i_slice];
            for (int k_row = 0; k_row < ny; k_row++) {
                *B_ptr = (*B_ptr + *reac_ptr * area * dy[k_row]) * mult;
                B_ptr++;
                reac_ptr++;
            }
        }
    
    if (scale_factor[0] != IGNORE)
        free(dt_vec);
}

//timefrac: 0 = explicit, 1 < timefrac < 0 = semi-implicit, 1 = implicity
//dt is timestep, init is old concentrations organized in fist columns and then slices
//new_conc is only used for explicit method, when solution can be obtained wihtout..
// ... matrix inversion.
//for other variables see description compose_sscoefmat_3D
void compose_transcoefmat_3D(int * diff_method,
                             double * Dx, double * ux, //diffcoefs & adv x-direction
                             double * Dy, double * uy, //...  y-direction
                             double * Dz, double * uz, //... z-direc
                             double * reac,
                             double * ratecoef,
                             double * scale_factor,
                             int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                             int * westbnd_type, double * westbnd_val, int * singleBndWest,
                             int * upbnd_type, double * upbnd_val, int * singleBndUp,
                             int * downbnd_type, double * downbnd_val, int * singleBndDown,
                             int * northbnd_type, double * northbnd_val, int * singleBndNorth,
                             int * southbnd_type, double * southbnd_val, int * singleBndSouth,
                             int * n, double * dx, double * dy, double * dz,
                             double * A_mats_horz, double * A_mats_vert,
                             double * A_mats_depth, double * B,
                             double * timefrac, double * dt, double * init) {
    int nx = n[0];
    int ny = n[1];
    int nz = n[2];
    
    //obtain coefficient matrixes for old state variables
    int old = 1;
    double * rhs = (double *)malloc(nx*ny*nz*sizeof(double));
    
    if (timefrac[0] != 1) {
        steady_to_transient_3D(diff_method,
                               Dx, ux, //diffcoefs & adv x-direction
                               Dy, uy, //...  y-direction
                               Dz, uz, //... z-direc
                               reac,
                               ratecoef,
                               scale_factor,
                               eastbnd_type, eastbnd_val, singleBndEast,
                               westbnd_type, westbnd_val, singleBndWest,
                               upbnd_type, upbnd_val, singleBndUp,
                               downbnd_type, downbnd_val, singleBndDown,
                               northbnd_type, northbnd_val, singleBndNorth,
                               southbnd_type, southbnd_val, singleBndSouth,
                               n, dx, dy, dz,
                               A_mats_horz, A_mats_vert,
                               A_mats_depth, B,
                               timefrac, dt, &old);
        
        //A * new_time_statevars - S_new = B * previous_time_statevars - S_old
        //Solve right-hand side
        //first info from all vertical matrices
        //also add time derivative (+diag(1, ny)) to vertical matrices
        int rhs_index = 0;
        int ap_vert = 0;
        for (int i_slice = 0; i_slice < nz; i_slice++) {
            for (int j_col = 0; j_col < nx; j_col++) {
                rhs[rhs_index] = -B[rhs_index] +
                    A_mats_vert[ap_vert] * init[rhs_index] + //vertical ap * Cp
                    A_mats_vert[ap_vert+1] * init[rhs_index+1]; //vertical as * Cs (cell below)
                rhs_index++;
                ap_vert = ap_vert + 3;
                for (int k_row = 1; k_row < (ny-1); k_row++) {
                    rhs[rhs_index] = -B[rhs_index] +
                        A_mats_vert[ap_vert-1] * init[rhs_index-1] + //vertical an * Cn (cell above)
                        A_mats_vert[ap_vert] * init[rhs_index] + //vertical ap * Cp
                        A_mats_vert[ap_vert+1] * init[rhs_index+1]; //vertical as * Cs (cell below)
                    rhs_index++;
                    ap_vert = ap_vert + 3;
                }
                rhs[rhs_index] = -B[rhs_index] +
                    A_mats_vert[ap_vert-1] * init[rhs_index-1] + //vertical an * Cn (cell above)
                    A_mats_vert[ap_vert] * init[rhs_index]; //vertical ap * Cp
                rhs_index++;
                ap_vert = ap_vert + 1;
            }
        }
        
        //info from all horizontal matrices
        int ap_horz = 0;
        int rhs_w, rhs_p, rhs_e;
        for (int i_slice = 0; i_slice < nz; i_slice++) {
            for (int k_row = 0; k_row < ny; k_row++) {
                rhs_p = k_row + i_slice * nx * ny;
                rhs_e = rhs_p + ny;
                rhs[rhs_p] = rhs[rhs_p] +
                    A_mats_horz[ap_horz] * init[rhs_p] +
                    A_mats_horz[ap_horz+1] * init[rhs_e]; //east cell
                ap_horz = ap_horz + 3;
                for (int j_col = 1; j_col < (nx-1); j_col++) {
                    rhs_w = rhs_p;
                    rhs_p = rhs_p + ny;
                    rhs_e = rhs_p + ny;
                    rhs[rhs_p] = rhs[rhs_p] +
                        A_mats_horz[ap_horz-1] * init[rhs_w] + //west cell
                        A_mats_horz[ap_horz] * init[rhs_p] +
                        A_mats_horz[ap_horz+1] * init[rhs_e]; //east cell
                    ap_horz = ap_horz + 3;
                }
                rhs_w = rhs_p;
                rhs_p = rhs_p + ny;
                rhs[rhs_p] = rhs[rhs_p] +
                    A_mats_horz[ap_horz-1] * init[rhs_w] + //west cell
                    A_mats_horz[ap_horz] * init[rhs_p];
                ap_horz = ap_horz + 1;
            }
        }
        //info from all depth matrices
        int ap_depth = 0;
        int incdepth = ny * nx;
        for (int j_col = 0; j_col < nx; j_col++) {
            for (int k_row = 0; k_row < ny; k_row++) {
                rhs_p = k_row + j_col * ny;
                rhs_e = rhs_p + incdepth;
                rhs[rhs_p] = rhs[rhs_p] +
                    A_mats_depth[ap_depth] * init[rhs_p] +
                    A_mats_depth[ap_depth+1] * init[rhs_e]; //east cell
                ap_depth = ap_depth + 3;
                for (int i_slice = 1; i_slice < (nz-1); i_slice++) {
                    rhs_w = rhs_p;
                    rhs_p = rhs_p + incdepth;
                    rhs_e = rhs_p + incdepth;
                    rhs[rhs_p] = rhs[rhs_p] +
                    A_mats_depth[ap_depth-1] * init[rhs_w] + //west cell
                        A_mats_depth[ap_depth] * init[rhs_p] +
                        A_mats_depth[ap_depth+1] * init[rhs_e]; //east cell
                    ap_depth = ap_depth + 3;
                }
                rhs_w = rhs_p;
                rhs_p = rhs_p + incdepth;
                rhs[rhs_p] = rhs[rhs_p] +
                    A_mats_depth[ap_depth-1] * init[rhs_w] +
                    A_mats_depth[ap_depth] * init[rhs_p]; //east cell
                ap_depth = ap_depth + 1;
            }
        }
    } else { //for implicit rhs is simply C * volume
      double * rhs_ptr = &rhs[0];
      double * init_ptr = &init[0];
      for (int i_slice = 0; i_slice < nz; i_slice++) {
          for (int j_col = 0; j_col < nx; j_col++) {
              double area = dz[i_slice] * dx[j_col];
              for (int k_row = 0; k_row < ny; k_row++) {
                  rhs_ptr[0] = init_ptr[0] * area * dy[k_row];
                  rhs_ptr++;
                  init_ptr++;
              }
          }
      }
    }
    
    if (timefrac[0] == 0) { //explicit
        //solution depends only on old concentrations, i.e. lhs would be identity matrix * volume
        double * new_conc_ptr = &init[0];
        double * rhs_ptr = &rhs[0];
        for (int i_slice = 0; i_slice < nz; i_slice++) {
          for (int j_col = 0; j_col < nx; j_col++) {
            double area = dz[i_slice] * dx[j_col];
            for (int k_row = 0; k_row < ny; k_row++) {
              *new_conc_ptr = *rhs_ptr / (area * dy[k_row]);
              new_conc_ptr++;
              rhs_ptr++;
            }
          }
        }
    } else { //semi-implicit or implicit
        //solution cannot be obtained without matrix inversion
        old = 0; //FALSE for new matrices
        
        //obtain matrices for new concentrations, left-hand side
        steady_to_transient_3D(diff_method,
                               Dx, ux, //diffcoefs & adv x-direction
                               Dy, uy, //...  y-direction
                               Dz, uz, //... z-direc
                               reac,
                               ratecoef,
                               scale_factor,
                               eastbnd_type, eastbnd_val, singleBndEast,
                               westbnd_type, westbnd_val, singleBndWest,
                               upbnd_type, upbnd_val, singleBndUp,
                               downbnd_type, downbnd_val, singleBndDown,
                               northbnd_type, northbnd_val, singleBndNorth,
                               southbnd_type, southbnd_val, singleBndSouth,
                               n, dx, dy, dz,
                               A_mats_horz, A_mats_vert,
                               A_mats_depth, B,
                               timefrac, dt, &old);
        //combine source term new matrices with rhs matrix
        //solver in Jacobi assumes B is added for each dimension, therefore multiplication by 3
        for (int i = 0; i < nx*nz*ny; i++) {	
            B[i] = -B[i] + rhs[i];
        }
    }
    free(rhs);
}

//this functions allows to solve a 1D steady-state system in one step, which
// decreases the traffic between R and this library
void steadyall_1D(int * diff_method,
                  double * D, double * u, double * S,
                  int * nx, int * upbnd_type, double * upbnd_val,
                  int * lowbnd_type, double * lowbnd_val,
                  double * ratecoef, double * dx, double * result) {
    
    double * coefmat = (double *)malloc(sizeof(double)*(nx[0]*3-2));
    compose_sscoefmat_1D(diff_method, D, u, S, nx, upbnd_type, upbnd_val,
                         lowbnd_type, lowbnd_val, ratecoef, dx, coefmat);
    solveTri(nx, coefmat, S, result);
    free(coefmat);
}

//see compose_transcoefmat_3D for commentary on code
void transientall_1D(int * diff_method,
                     double * D, double * u,
                     double * reac, double * ratecoef,
                     double * scale_factor,
                     int * upbnd_type, double * upbnd_val,
                     int * lowbnd_type, double * lowbnd_val,
                     int * nx, double * dx,
                     double * timefrac, double * dt,
                     double * init, double * new_conc) {
    int old = 1;
    //copy reaction rates
    double * rhs = (double * )malloc(nx[0] * sizeof(double));
    for(int i = 0; i < nx[0]; i++)
        rhs[i] = reac[i];
    
    double * A = (double *)malloc( (nx[0]*3-2) * sizeof(double));
    
    //rhs will be adapted to account for boundary condition, time, etc
    steady_to_transient_1D(diff_method, D, u, rhs, nx,
                           upbnd_type, upbnd_val, lowbnd_type, lowbnd_val,
                           ratecoef, scale_factor, dx, A, timefrac, dt, &old);
    
    double * rvec = (double *)malloc(nx[0] * sizeof(double));
    
    //multply init * A and substract rhs
    rvec[0] = init[0] * A[0] + init[1] * A[1] - rhs[0];
    for (int i = 1; i < (nx[0]-1); i++) {
        int ap = i * 3;
        rvec[i] = init[i-1] * A[ap-1] + init[i] * A[ap] + init[i+1] * A[ap+1] - rhs[i];
    }
    int ap = (nx[0]-1)*3;
    rvec[nx[0]-1] = init[nx[0]-2] * A[ap-1] + init[nx[0]-1] * A[ap] - rhs[nx[0]-1];
    
    free(rhs);
    if (timefrac[0] == 0) { //explicit
        for (int i = 0; i < nx[0]; i++) {
            new_conc[i] = rvec[i]/dx[i];
        }
    } else { //(semi-)implicit
        old = 0;
        steady_to_transient_1D(diff_method, D, u, reac, nx,
                               upbnd_type, upbnd_val, lowbnd_type, lowbnd_val,
                               ratecoef, scale_factor, dx, A, timefrac, dt, &old);
        //reac is adapted to account for b.c. etc
        //A contains coefmat
        for (int i = 0; i < nx[0]; i++)
            rvec[i] = rvec[i] - reac[i];
        //invert matrix and solve the system
        solveTri(nx, A, rvec, new_conc);
    }
    free(A);
    free(rvec);
}

// see compose_sscoefmat_2D for first variables
// see solve2D for other variables
// conc is the initial guess and the final result will be stored in it
void steadyall_2D(int * diff_method,
                  double * Dx, double * ux, //diffcoefs & adv x-direction
                  double * Dy, double * uy, //...  y-direction
                  double * reac, double * ratecoef,
                  int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                  int * westbnd_type, double * westbnd_val, int * singleBndWest,
                  int * upbnd_type, double * upbnd_val, int * singleBndUp,
                  int * downbnd_type, double * downbnd_val, int * singleBndDown,
                  int * n, double * dx, double * dy,
                  double * conc, int * max_iterations, double * rel_tol,
                  int * solveCols, int * sweep_direction) {
    
    int multByArea = 1;
    
    double * A_mats_horz = (double *)malloc(sizeof(double) * (n[0]*3-2)*n[1]);
    double * A_mats_vert = (double *)malloc(sizeof(double) * (n[1]*3-2)*n[0]);
    double * B = (double *)malloc(sizeof(double) * n[0] * n[1]);
    
    compose_sscoefmat_2D(diff_method, Dx, ux, Dy, uy,
                         &multByArea, reac, ratecoef,
                         eastbnd_type, eastbnd_val, singleBndEast,
                         westbnd_type, westbnd_val, singleBndWest,
                         upbnd_type, upbnd_val, singleBndUp,
                         downbnd_type, downbnd_val, singleBndDown,
                         n, dx, dy, A_mats_horz, A_mats_vert, B);
    
    solve2D(&n[1], &n[0], A_mats_horz, A_mats_vert,
            B, conc, max_iterations, rel_tol, solveCols,
            sweep_direction);
    
    free(A_mats_horz);
    free(A_mats_vert);
    free(B);
}

//conc has to be of the size nx * ny
void transientall_2D(int * diff_method,
                     double * Dx, double * ux,
                     double * Dy, double * uy,
                     double * reac,
                     double * ratecoef,
                     double * scale_factor,
                     int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                     int * westbnd_type, double * westbnd_val, int * singleBndWest,
                     int * upbnd_type, double * upbnd_val, int * singleBndUp,
                     int * downbnd_type, double * downbnd_val, int * singleBndDown,
                     int * n, double * dx, double * dy,
                     double * timefrac, double * dt, double * init,
                     int * max_iterations, double * rel_tol,
                     int * solveCols, int * sweep_direction) {
    
    double * A_mats_horz = (double *)calloc((n[0]*3-2)*n[1], sizeof(double));
    double * A_mats_vert = (double *)calloc((n[1]*3-2)*n[0], sizeof(double));
    double * B = (double *)calloc(n[0] * n[1], sizeof(double));
    
    compose_transcoefmat_2D(diff_method,
                            Dx, ux, Dy, uy,
                            reac, ratecoef, scale_factor,
                            eastbnd_type, eastbnd_val, singleBndEast,
                            westbnd_type, westbnd_val, singleBndWest,
                            upbnd_type, upbnd_val, singleBndUp,
                            downbnd_type, downbnd_val, singleBndDown,
                            n, dx, dy, A_mats_horz, A_mats_vert,
                            B, timefrac, dt, init);
    
    //in case of explicit discretization the solution is already obtained
    if (timefrac[0] != 0) {
        solve2D(&n[1], &n[0], A_mats_horz, A_mats_vert,
                B, init, max_iterations, rel_tol, solveCols,
                sweep_direction);
    }
    
    free(A_mats_horz);
    free(A_mats_vert);
    free(B);
}

void steadyall_3D(int * diff_method,
                  double * Dx, double * ux, //diffcoefs & adv x-direction
                  double * Dy, double * uy, //...  y-direction
                  double * Dz, double * uz, //... z-direc
                  double * reac, double * ratecoef,
                  int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                  int * westbnd_type, double * westbnd_val, int * singleBndWest,
                  int * upbnd_type, double * upbnd_val, int * singleBndUp,
                  int * downbnd_type, double * downbnd_val, int * singleBndDown,
                  int * northbnd_type, double * northbnd_val, int * singleBndNorth,
                  int * southbnd_type, double * southbnd_val, int * singleBndSouth,
                  int * n, double * dx, double * dy, double * dz,
                  double * conc, int * max_iterations, double * rel_tol,
                  int * sweep) {
    
    int multByArea = 1;
    
    double * A_mats_horz = (double *)malloc(sizeof(double) * (n[0]*3-2)*n[1]*n[2]);
    double * A_mats_vert = (double *)malloc(sizeof(double) * (n[1]*3-2)*n[0]*n[2]);
    double * A_mats_depth = (double *)malloc(sizeof(double) * (n[2]*3-2)*n[0]*n[1]);
    double * B = (double *)malloc(sizeof(double) * n[0] * n[1] * n[2]);
    
    compose_sscoefmat_3D(diff_method, Dx, ux, Dy, uy, Dz, uz,
                         &multByArea, reac, ratecoef,
                         eastbnd_type, eastbnd_val, singleBndEast,
                         westbnd_type, westbnd_val, singleBndWest,
                         upbnd_type, upbnd_val, singleBndUp,
                         downbnd_type, downbnd_val, singleBndDown,
                         northbnd_type, northbnd_val, singleBndNorth,
                         southbnd_type, southbnd_val, singleBndSouth,
                         n, dx, dy, dz,
                         A_mats_horz, A_mats_vert, A_mats_depth, B);
    
    if (*sweep == 1) {
        solve3D_asd(&n[1], &n[0], &n[2],
                    A_mats_horz, A_mats_vert, A_mats_depth,
                    B, conc, max_iterations, rel_tol);
    } else {
        solve3D(&n[1], &n[0], &n[2],
                A_mats_horz, A_mats_vert, A_mats_depth,
                B, conc, max_iterations, rel_tol);
    }
    
    free(A_mats_horz);
    free(A_mats_vert);
    free(A_mats_depth);
    free(B);
}

void transientall_3D(int * diff_method,
                     double * Dx, double * ux,
                     double * Dy, double * uy,
                     double * Dz, double * uz,
                     double * reac,
                     double * ratecoef,
                     double * scale_factor,
                     int * eastbnd_type, double * eastbnd_val, int * singleBndEast,
                     int * westbnd_type, double * westbnd_val, int * singleBndWest,
                     int * upbnd_type, double * upbnd_val, int * singleBndUp,
                     int * downbnd_type, double * downbnd_val, int * singleBndDown,
                     int * northbnd_type, double * northbnd_val, int * singleBndNorth,
                     int * southbnd_type, double * southbnd_val, int * singleBndSouth,
                     int * n, double * dx, double * dy, double * dz,
                     double * timefrac, double * dt, double * init,
                     int * max_iterations, double * rel_tol, int * sweep) {
    
    double * A_mats_horz = (double *)calloc((n[0]*3-2)*n[1]*n[2], sizeof(double));
    double * A_mats_vert = (double *)calloc((n[1]*3-2)*n[0]*n[2], sizeof(double));
    double * A_mats_depth = (double *)calloc((n[2]*3-2)*n[0]*n[1], sizeof(double));
    double * B = (double *)calloc(n[0] * n[1] * n[2], sizeof(double));
    
    compose_transcoefmat_3D(diff_method,
                            Dx, ux, Dy, uy, Dz, uz,
                            reac, ratecoef, scale_factor,
                            eastbnd_type, eastbnd_val, singleBndEast,
                            westbnd_type, westbnd_val, singleBndWest,
                            upbnd_type, upbnd_val, singleBndUp,
                            downbnd_type, downbnd_val, singleBndDown,
                            northbnd_type, northbnd_val, singleBndNorth,
                            southbnd_type, southbnd_val, singleBndSouth,
                            n, dx, dy, dz,
                            A_mats_horz, A_mats_vert, A_mats_depth,
                            B, timefrac, dt, init);
    
    //in case of explicit discretization the solution is already obtained
    if (timefrac[0] != 0) {
        if (sweep[0]) {
            solve3D_asd(&n[1], &n[0], &n[2], A_mats_horz, A_mats_vert, A_mats_depth,
                        B, init, max_iterations, rel_tol);
        } else {
            solve3D(&n[1], &n[0], &n[2], A_mats_horz, A_mats_vert, A_mats_depth,
                    B, init, max_iterations, rel_tol);
        }
    }
    
    free(A_mats_horz);
    free(A_mats_vert);
    free(A_mats_depth);
    free(B);

}

void cpf_1D(double * permvisc, double * dx, int * n,
            int * upbnd_type, double * upbnd_val,
            int * downbnd_type, double * downbnd_val,
            double * A_mats, double * B) {
    int nx = n[0];
    double * pv_ptr = permvisc;
    double * am_ptr = A_mats;
    double * dx_ptr = dx;
    double kx, twodx;
    
    //at first boundary before P cell
    twodx = (*dx_ptr);
    dx_ptr++;
    if (*upbnd_type == 2) { //Dirichlet
        kx = 2 * (*pv_ptr) / twodx;
        B[0] = kx * (*upbnd_val); //W
        *am_ptr = kx; //P
    } else if(*upbnd_type == 3) { //No gradient
        *am_ptr = 0.0;
        B[0] = 0.0; //same as fixed flux of 0
    } else if(*upbnd_type == 1) { //Fixed flux
        *am_ptr = 0.0;
        B[0] = (*upbnd_val);
    }
    
    //calculate next kx
    kx = (*pv_ptr);
    pv_ptr++;
    twodx = twodx + (*dx_ptr);
    kx = (kx + (*pv_ptr)) / twodx;
    //add to P an E cell
    *am_ptr = *am_ptr + kx; //P cell
    am_ptr++;
    *am_ptr = -kx; //E cell
    am_ptr++;
    //interior of A matrix
    for (int i_cell = 1; i_cell < (nx-1); ++i_cell) {
        *am_ptr = -kx; //W cell
        am_ptr++;
        *am_ptr = kx; //P cell
        kx = (*pv_ptr);
        pv_ptr++;
        twodx = (*dx_ptr);
        dx_ptr++;
        twodx = twodx + (*dx_ptr);
        kx = (kx + (*pv_ptr)) / twodx;
        *am_ptr = *am_ptr + kx; //P cell
        am_ptr++;
        *am_ptr = -kx; //E cell
        am_ptr++;
    }
    //last row of matrix
    *am_ptr = -kx; //W cell
    am_ptr++;
    *am_ptr = kx; //P cell
    
    if (*downbnd_type == 2) { //Dirichlet
        kx = 2 * (*pv_ptr) / (*dx_ptr);
        *am_ptr = *am_ptr + kx; //P cell
        B[nx-1] = kx * (*downbnd_val);
    } else if(*downbnd_type == 3) { //No gradient
        B[0] = 0.0; //same as fixed flux of 0
    } else if(*downbnd_type == 1) { //Fixed flux
        B[nx-1] = -(*downbnd_val);
    }
}

//permvisc is the permeability over viscosity
void cpf_2D(double * permvisc, double * dx, double * dy, int * n,
            int * eastbnd_type, double * eastbnd_val,
            int * westbnd_type, double * westbnd_val,
            int * upbnd_type, double * upbnd_val,
            int * downbnd_type, double * downbnd_val,
            double * A_mats_horz, double * A_mats_vert,
            double * B) {
    int nx = n[0];
    int ny = n[1];
    
    int size_A_row = nx * 3 -2;
    int size_A_col = ny * 3 -2;
    
    //make matrices for all columns
    int startA = 0;
    for (int j_col = 0; j_col < nx; ++j_col) {
        int startCol = j_col * ny;
        
        cpf_1D(&permvisc[startCol],
               dy, &ny,
               upbnd_type, upbnd_val,
               downbnd_type, downbnd_val,
               &A_mats_vert[startA], &B[startCol]);
        
        startA = startA + size_A_col;
    }
    
    //make matrices for all rows
    double * Sx = (double *)malloc(sizeof(double)*nx);
    for (int k = 0; k < nx; ++k) {
        Sx[k] = 0;
    }
    
    startA = 0;
    for (int j_row = 0; j_row < ny; ++j_row) {
        //double area = dy[j_row];
        
        double pv_horz[nx];
        for (int k =0; k < nx; ++k) {
            pv_horz[k] = permvisc[k*ny + j_row];
        }
        
        cpf_1D(pv_horz,
               dx, &nx,
               westbnd_type, westbnd_val,
               eastbnd_type, eastbnd_val,
               &A_mats_horz[startA], Sx);
        
        //copy boundary conditions to src vector
        for (int k = 0; k < nx; k++) {
            int index = k*ny + j_row;
            B[index] = B[index] + Sx[k];
            Sx[k] = 0;
        }
        
        startA = startA + size_A_row;
    }
    free(Sx);
}

//permvisc is the permeability over viscosity
void cpf_3D(double * permvisc, double * dx, double * dy,
            double * dz, int * n,
            int * eastbnd_type, double * eastbnd_val,
            int * westbnd_type, double * westbnd_val,
            int * upbnd_type, double * upbnd_val,
            int * downbnd_type, double * downbnd_val,
            int * northbnd_type, double * northbnd_val,
            int * southbnd_type, double * southbnd_val,
            double * A_mats_horz, double * A_mats_vert,
            double * A_mats_depth, double * B) {
    int nx = n[0];
    int ny = n[1];
    int nz = n[2];
    
    int size_A_row = nx * 3 - 2;
    int size_A_col = ny * 3 - 2;
    int size_A_depth = nz * 3 - 2;
    
    //make matrices for all columns
    int startA = 0;
    int startCol = 0;
    for (int i_slice = 0; i_slice < nz; ++i_slice) {
        for (int j_col = 0; j_col < nx; ++j_col) {
            //double area = dx[j_col] ;
            cpf_1D(&permvisc[startCol],
                   dy, &ny,
                   upbnd_type, upbnd_val,
                   downbnd_type, downbnd_val,
                   &A_mats_vert[startA], &B[startCol]);
            
            startA = startA + size_A_col;
            startCol = startCol + ny;
        }
    }
    
    //make matrices for all rows
    double Sx[nx];
    for (int k = 0; k < nx; ++k) {
        Sx[k] = 0;
    }
    
    startA = 0;
    for (int i_slice = 0; i_slice < nz; ++i_slice) {
        for (int j_row = 0; j_row < ny; ++j_row) {
            //double area = dy[j_row];
            
            double pv_horz[nx];
            for (int k_col =0; k_col < nx; ++k_col) {
                pv_horz[k_col] = permvisc[ i_slice * nx * ny + k_col*ny + j_row];
            }
            
            cpf_1D(pv_horz,
                   dx, &nx,
                   westbnd_type, westbnd_val,
                   eastbnd_type, eastbnd_val,
                   &A_mats_horz[startA], Sx);
            
            
            //copy boundary conditions to src vector
            for (int k_col = 0; k_col < nx; ++k_col) {
                int index = i_slice * nx * ny + k_col*ny + j_row;
                B[index] = B[index] + Sx[k_col];
                Sx[k_col] = 0;
            }
            startA = startA + size_A_row;
        }
    }
    
    //make matrices for all depth rows
    double Sz[nz];
    for (int k = 0; k < nz; ++k) {
        Sz[k] = 0;
    }
    
    startA = 0;
    for (int i_col = 0; i_col < nx; ++i_col) {
        for (int j_row = 0; j_row < ny; ++j_row) {
            //double area = dy[j_row];
            
            double pv_vert[nz];
            for (int k_depth =0; k_depth < nz; ++k_depth) {
                pv_vert[k_depth] = permvisc[k_depth * nx * ny + i_col*ny + j_row];
            }
            
            cpf_1D(pv_vert,
                   dz, &nz,
                   northbnd_type, northbnd_val,
                   southbnd_type, southbnd_val,
                   &A_mats_depth[startA], Sz);
            
            //copy boundary conditions to src vector
            for (int k_depth = 0; k_depth < nz; ++k_depth) {
                int index = k_depth * nx * ny + i_col*ny + j_row;
                B[index] = B[index] + Sz[k_depth];
                Sz[k_depth] = 0;
            }
            startA = startA + size_A_depth;
        }
    }
}

void pres1D_all(double * permvisc, double * dx, int * n,
                int * upbnd_type, double * upbnd_val,
                int * downbnd_type, double * downbnd_val,
                double * result) {
    double * A_mats, * B;
    
    A_mats = (double *)malloc(sizeof(double)*(n[0]*3-2));
    B = (double *)calloc(n[0], sizeof(double));
    
    cpf_1D(permvisc,
           dx, n,
           upbnd_type, upbnd_val,
           downbnd_type, downbnd_val,
           A_mats, B);
    solveTri(n, A_mats, B, result);
    
    free(A_mats);
    free(B);
}

void calc_darcy1D(double * permvisc, double * dx, int * n,
                  int * upbnd_type, double * upbnd_val,
                  int * downbnd_type, double * downbnd_val,
                  double * pres, double * result) {
    double * pv_ptr, * dx_ptr;
    double kx, twodx;
    
    //first boundary
    pv_ptr = permvisc;
    kx = *pv_ptr;
    dx_ptr = dx;
    twodx = *dx_ptr; //first dx
    if (*upbnd_type == 2) { //Dirichlet
        result[0] = - 2 * kx * (pres[0] - (*upbnd_val)) / twodx;
    } else if (*upbnd_type == 1) {  //Fixed flux
        result[0] = *upbnd_val;
    } else { //no gradient
        result[0] = 0.0;
    }
    
    //interior grid boundaries
    pv_ptr++;
    dx_ptr++;
    for (int i_bnd = 1; i_bnd < n[0]; ++i_bnd ) {
        kx = kx + (*pv_ptr);
        twodx = twodx + (*dx_ptr);
        result[i_bnd] = - kx * (pres[i_bnd] - pres[i_bnd-1]) / twodx;
        twodx = (*dx_ptr);
        dx_ptr++;
        kx = (*pv_ptr);
        pv_ptr++;
    }
    
    //last boundary
    if (*downbnd_type == 2) { //Dirichlet
        result[n[0]] = - 2 * kx * ((*downbnd_val) - pres[n[0]-1]) / twodx;
    } else if (*upbnd_type == 1) {  //Fixed flux
        result[n[0]] = *downbnd_val;
    } else { //no gradient
        result[n[0]] = 0.0;
    }
}

void darcy1D_all(double * permvisc, double * dx, int * n,
                 int * upbnd_type, double * upbnd_val,
                 int * downbnd_type, double * downbnd_val,
                 double * result) {
    double * pres = (double *)malloc(sizeof(double)*(n[0]));
    
    pres1D_all(permvisc, dx, n,
               upbnd_type, upbnd_val,
               downbnd_type, downbnd_val,
               pres);
    
    calc_darcy1D(permvisc, dx, n,
                 upbnd_type, upbnd_val,
                 downbnd_type, downbnd_val,
                 pres, result);
    
    free(pres);
}

void pres2D_all(double * permvisc, double * dx, double * dy, int * n,
                int * eastbnd_type, double * eastbnd_val,
                int * westbnd_type, double * westbnd_val,
                int * upbnd_type, double * upbnd_val,
                int * downbnd_type, double * downbnd_val,
                double * conc, int * max_iterations, double * rel_tol,
                int * solveCols, int * sweep_direction) {
    double * A_mats_horz, * A_mats_vert, * B;
    
    A_mats_horz = (double *)malloc(sizeof(double)*(n[0]*3-2)*n[1]);
    A_mats_vert = (double *)malloc(sizeof(double)*(n[1]*3-2)*n[0]);
    B = (double *)calloc(n[0]*n[1], sizeof(double));
    
    cpf_2D(permvisc, dx, dy, n,
           eastbnd_type, eastbnd_val,
           eastbnd_type, westbnd_val,
           upbnd_type, upbnd_val,
           downbnd_type, downbnd_val,
           A_mats_horz, A_mats_vert, B);
    
    solve2D(&n[1], &n[0], A_mats_horz, A_mats_vert,
            B, conc, max_iterations, rel_tol, solveCols,
            sweep_direction);
    
    free(A_mats_horz);
    free(A_mats_vert);
    free(B);
}

void pres3D_all(double * permvisc, double * dx, double * dy,
                double * dz, int * n,
                int * eastbnd_type, double * eastbnd_val,
                int * westbnd_type, double * westbnd_val,
                int * upbnd_type, double * upbnd_val,
                int * downbnd_type, double * downbnd_val,
                int * northbnd_type, double * northbnd_val,
                int * southbnd_type, double * southbnd_val,
                double * pres, int * max_iterations, double * rel_tol) {
    double * A_mats_horz, * A_mats_vert, * A_mats_depth, * B;
    
    A_mats_horz = (double *)malloc(sizeof(double)*(n[0]*3-2)*n[1]*n[2]);
    A_mats_vert = (double *)malloc(sizeof(double)*(n[1]*3-2)*n[0]*n[2]);
    A_mats_depth = (double *)malloc(sizeof(double)*(n[2]*3-2)*n[0]*n[1]);
    B = (double *)calloc(n[0]*n[1]*n[2], sizeof(double));
    
    cpf_3D(permvisc,
           dx, dy, dz, n,
           eastbnd_type, eastbnd_val,
           westbnd_type, westbnd_val,
           upbnd_type, upbnd_val,
           downbnd_type, downbnd_val,
           northbnd_type, northbnd_val,
           southbnd_type, southbnd_val,
           A_mats_horz, A_mats_vert,
           A_mats_depth, B);
    
    solve3D_asd(&n[1], &n[0], &n[2],
                A_mats_horz, A_mats_vert,
                A_mats_depth, B,
                pres, max_iterations, rel_tol);
    
    free(A_mats_horz);
    free(A_mats_vert);
    free(A_mats_depth);
    free(B);
}

void calc_darcy2D(double * permvisc, double * dx, double * dy, int * n,
                  int * eastbnd_type, double * eastbnd_val,
                  int * westbnd_type, double * westbnd_val,
                  int * upbnd_type, double * upbnd_val,
                  int * downbnd_type, double * downbnd_val,
                  double * pres, double * qx, double * qy) {
    int nx = n[0];
    int ny = n[1];
    
    //calculate darcy in y direction
    int start_node = 0;
    int start_bnd = 0;
    for (int i_col = 0; i_col < nx; ++i_col) {
        calc_darcy1D(&permvisc[start_node], dy, &ny,
                     upbnd_type, upbnd_val,
                     downbnd_type, downbnd_val,
                     &pres[start_node], &qy[start_bnd]);
        start_node = start_node + ny;
        start_bnd = start_bnd + ny + 1;
    }
    
    //calculate darcy in x dir
    double * qx_ptr = &qx[0];
    for (int j_row = 0; j_row < ny; ++j_row) {
        //first boundary
        if (*westbnd_type == 2) { //Dirichlet
            *qx_ptr = - 2 * permvisc[j_row] * (pres[j_row] - (*westbnd_val)) / dx[0];
        } else if (*westbnd_type == 1) {  //Fixed flux
            *qx_ptr = *westbnd_val;
        } else { //no gradient
            *qx_ptr = 0.0;
        }
        qx_ptr++;
    }
    
    //make two pointers for horizontally adjacent cells
    double * pv1_ptr = permvisc;
    double * pv2_ptr = &permvisc[ny];
    double * pres1_ptr = pres;
    double * pres2_ptr = &pres[ny];
    
    for (int i_bnd = 1; i_bnd < nx; ++i_bnd) {
        double twodx = dx[i_bnd-1] + dx[i_bnd];
        for (int j_row = 0; j_row < ny; ++j_row) {
            *qx_ptr = - ( *pv1_ptr + *pv2_ptr ) * ( pres2_ptr[0] - pres1_ptr[0]) / twodx;
            //increase pointers
            qx_ptr++;
            pv1_ptr++;
            pv2_ptr++;
            pres1_ptr++;
            pres2_ptr++;
        }
    }
    
    //last boundary
    for (int j_row = 0; j_row < ny; ++j_row) {
        if (*eastbnd_type == 2) { //Dirichlet
            *qx_ptr = -2 * (*pv1_ptr) * ( *eastbnd_val - *pres1_ptr ) / dx[nx-1];
            pv1_ptr++;
            pres1_ptr++;
        } else if (*eastbnd_type == 1) { //Fixed flux
            *qx_ptr = *eastbnd_val;
        } else { //No gradient
            *qx_ptr = 0.0;
        }
        qx_ptr++;
    }
}

void darcy2D_all(double * permvisc, double * dx, double * dy, int * n,
                 int * eastbnd_type, double * eastbnd_val,
                 int * westbnd_type, double * westbnd_val,
                 int * upbnd_type, double * upbnd_val,
                 int * downbnd_type, double * downbnd_val,
                 double * pres,
                 int * max_iterations, double * rel_tol,
                 double * qx, double * qy, int * solveCols,
                 int * sweep_direction) {
    pres2D_all(permvisc, dx, dy, n,
               eastbnd_type, eastbnd_val,
               westbnd_type, westbnd_val,
               upbnd_type, upbnd_val,
               downbnd_type, downbnd_val,
               pres, max_iterations, rel_tol,
               solveCols, sweep_direction);
    
    calc_darcy2D(permvisc, dx, dy, n,
                 eastbnd_type, eastbnd_val,
                 westbnd_type, westbnd_val,
                 upbnd_type, upbnd_val,
                 downbnd_type, downbnd_val,
                 pres, qx, qy);
}

void darcy3D_all(double * permvisc, double * dx, double * dy,
                 double * dz, int * n,
                 int * eastbnd_type, double * eastbnd_val,
                 int * westbnd_type, double * westbnd_val,
                 int * upbnd_type, double * upbnd_val,
                 int * downbnd_type, double * downbnd_val,
                 int * northbnd_type, double * northbnd_val,
                 int * southbnd_type, double * southbnd_val,
                 double * pres,
                 int * max_iterations, double * rel_tol,
                 double * qx, double * qy, double * qz) {
    int nx = n[0];
    int ny = n[1];
    int nz = n[2];

    pres3D_all(permvisc, dx, dy,
               dz, n,
               eastbnd_type, eastbnd_val,
               westbnd_type, westbnd_val,
               upbnd_type, upbnd_val,
               downbnd_type, downbnd_val,
               northbnd_type, northbnd_val,
               southbnd_type, southbnd_val,
               pres, max_iterations, rel_tol);

    //calculate darcy velocity in x and y directions
    int start_node = 0;
    int start_qx = 0;
    int start_qy = 0;
    for (int i_slice = 0; i_slice < nz; ++i_slice) {
        calc_darcy2D(&permvisc[start_node], dx, dy, n,
                     eastbnd_type, eastbnd_val,
                     westbnd_type, westbnd_val,
                     upbnd_type, upbnd_val,
                     downbnd_type, downbnd_val,
                     &pres[start_node],
                     &qx[start_qx], &qy[start_qy]);
        
        start_node = start_node + nx * ny;
        start_qx = start_qx + (nx+1) * ny;
        start_qy = start_qy + (ny+1) * nx;
    }
    
    //calculate darcy in z dir
    double * qz_ptr = qz;
    //front slice
    for (int i_col = 0; i_col < nx; ++i_col) {
        for (int j_row = 0; j_row < ny; ++j_row) {
            //first boundary
            if (*northbnd_type == 2) { //Dirichlet
                int node_index = j_row + i_col * ny;
                *qz_ptr = - 2 * permvisc[node_index] * (pres[node_index] - (*northbnd_val)) / dz[0];
            } else if (*northbnd_type == 1) {  //Fixed flux
                *qz_ptr = *northbnd_val;
            } else { //no gradient
                *qz_ptr = 0.0;
            }
            qz_ptr++;
        }
    }
    
    //make two pointers for adjacent cells in depth direction
    double * pv1_ptr = permvisc; //first slice
    double * pv2_ptr = &permvisc[nx*ny]; //second slice
    double * pres1_ptr = pres; //first slice
    double * pres2_ptr = &pres[nx*ny]; //second slice
    
    for (int i_bnd = 1; i_bnd < nz; ++i_bnd) {
        double twodz = dz[i_bnd-1] + dz[i_bnd];
        for (int i_col = 0; i_col < nx; ++i_col) {
            for (int j_row = 0; j_row < ny; ++j_row) {
                *qz_ptr = - ( *pv1_ptr + *pv2_ptr ) * ( pres2_ptr[0] - pres1_ptr[0]) / twodz;
                //increase pointers
                qz_ptr++;
                pv1_ptr++;
                pv2_ptr++;
                pres1_ptr++;
                pres2_ptr++;
            }
        }
    }
    
    //last boundary
    for (int i_col = 0; i_col < nx; ++i_col) {
        for (int j_row = 0; j_row < ny; ++j_row) {
            if (*southbnd_type == 2) { //Dirichlet
                *qz_ptr = -2 * (*pv1_ptr) * ( *southbnd_val - *pres1_ptr ) / dz[nz-1];
                pv1_ptr++;
                pres1_ptr++;
            } else if (*southbnd_type == 1) { //Fixed flux
                *qz_ptr = *southbnd_val;
            } else { //No gradient
                *qz_ptr = 0.0;
            }
            qz_ptr++;
        }
    }
}
